package de.tu_bs.cs.isf.temporalregulator3000.ui.treeview;


import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.jface.viewers.ITreeContentProvider;

public class TreeContentProviderImplCustom implements ITreeContentProvider {
	@Override
	public boolean hasChildren(Object element) {
		if (element instanceof EPackage)
			if (((EPackage) element).eContents().size() > 0)
				return true;

		if (element instanceof EClass)
			if (((EClass) element).getEStructuralFeatures().size() > 0)
				return true;

		return false;
	}

	@Override
	public Object getParent(Object element) {
		if (element instanceof EObject)
			return ((EObject) element).eContainer();
		return null;
	}

	@Override
	public Object[] getElements(Object inputElement) {
		if (inputElement instanceof EPackage)
			return getEContentsFromEPackage((EPackage) inputElement);
		if (inputElement instanceof EClass)
			return ((EClass) inputElement).getEStructuralFeatures().toArray();
		return new Object[0];
	}

	@Override
	public Object[] getChildren(Object parentElement) {
		if (parentElement instanceof EPackage)
			return getEContentsFromEPackage((EPackage) parentElement);
		if (parentElement instanceof EClass)
			return ((EClass) parentElement).getEStructuralFeatures().toArray();
		return new Object[0];
	}
	
	private Object[] getEContentsFromEPackage(EPackage p) {
		List<Object> list = new ArrayList<>();
		
		for(EPackage subP : p.getESubpackages())
			list.add(subP);
		
		for(EClassifier c : p.getEClassifiers())
			if(c instanceof EClass)
				list.add(c);
		
		return list.toArray();
	}
}