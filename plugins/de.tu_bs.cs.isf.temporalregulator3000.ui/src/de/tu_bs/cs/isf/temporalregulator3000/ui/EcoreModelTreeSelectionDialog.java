package de.tu_bs.cs.isf.temporalregulator3000.ui;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.jface.viewers.CheckboxTreeViewer;
import org.eclipse.jface.viewers.ColumnViewerToolTipSupport;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.dialogs.CheckedTreeSelectionDialog;

import de.tu_bs.cs.isf.temporalregulator3000.core.DialogUtil;

public class EcoreModelTreeSelectionDialog extends CheckedTreeSelectionDialog {
	private CheckboxTreeViewer viewer;
    private ITreeContentProvider contentProvider;
	private ModelConverterDialogHandler modelConverterDialogHandler;
	
	private List<EClass> allClasses;

	public EcoreModelTreeSelectionDialog(Shell parent, ILabelProvider labelProvider, ITreeContentProvider contentProvider) {
		super(parent, labelProvider, contentProvider);
		
		this.contentProvider = contentProvider;
	}

	public void setHandler(ModelConverterDialogHandler handler) {
		this.modelConverterDialogHandler = handler;
	}
	
	@Override
	protected CheckboxTreeViewer createTreeViewer(Composite parent) {
		viewer = super.createTreeViewer(parent);
		ColumnViewerToolTipSupport.enableFor(viewer);
		
		return viewer;
	}
	
	@Override
	public void setInput(Object input) {
		super.setInput(input);
		allClasses = getAllClasses((EObject) input);
	}

	private List<EClass> getAllClasses(EObject input) {
		List<EClass> returnList = new ArrayList<>();
		
		for(EObject e : input.eContents()) {
			if(e instanceof EClass)
				returnList.add((EClass) e);
			if(e instanceof EPackage)
				returnList.addAll(getAllClasses(e));
		}
		
		return returnList;
	}
	
	

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		super.createButtonsForButtonBar(parent);

		getOkButton().setText("Convert");
		getOkButton().setEnabled(false);
	}

	@Override
	protected void updateButtonsEnableState(IStatus status) {
		evaluateCheckedElements();

		super.updateButtonsEnableState(status);
		
		Button okButton = getOkButton();
		if(viewer.getCheckedElements().length > 0) {
			okButton.setEnabled(true);
		} else {
			okButton.setEnabled(false);
		}
	}

	@Override
	protected void okPressed() {
		boolean addedAnElement = false;
		for (Object checkedElement : viewer.getCheckedElements()) {
			if (!isGrayed(checkedElement)) {
				modelConverterDialogHandler.addElement((EObject) checkedElement);
				addedAnElement = true;
			}
		}
		
		if (!addedAnElement) {
			DialogUtil.getInstance().openWarningDialog("You need to select at least one element from your meta model. Selected elements will be converted to temporal elements.\n\nPlease try that again :)");
		}
		else {
			super.okPressed();
			modelConverterDialogHandler.convertAndSave();
		}
	}

	private void evaluateCheckedElements() {
		for (Object grayedElement : viewer.getGrayedElements()) {
			viewer.setChecked(grayedElement, false);
			viewer.setGrayed(grayedElement, false);
		}
		
		Set<Object> alreadyChecked = new HashSet<>();

		for(Object checkedElement : viewer.getCheckedElements()) {
			if(alreadyChecked.contains(checkedElement))
				continue;
			
			if(checkedElement instanceof EReference) {
				EReference opposite = ((EReference) checkedElement).getEOpposite();
				if(opposite != null) {
					alreadyChecked.add(opposite);
					
					// gray check references opposite to a reference that is checked for temporalization.
					viewer.setChecked(opposite, true);
					viewer.setGrayed(opposite, true);
				}
			}
//			else if (checkedElement instanceof EClass) {
//				for (EClass extendingClassifier : getExtendingClassifiers((EClass) checkedElement)) {
//					// gray check all classes which extend a classifier that is checked for temporalization.
//					viewer.setGrayed(extendingClassifier, true);
//					viewer.setChecked(extendingClassifier, true);
//				}
//			}
			
			alreadyChecked.add(checkedElement);
		}
		
		for(EClass eClass : allClasses) {
			for(EStructuralFeature feature : eClass.getEStructuralFeatures()) {
				if(!feature.isChangeable() || feature.isDerived()) {
					// uncheck when a feature is not changeable or derived.
					viewer.setGrayed(feature, true);
					viewer.setChecked(feature, false);
				}
			}
		}
	}
	
//	private Set<EClass> getExtendingClassifiers(EClass superClassifier) {
//		Set<EClass> extendingClassifiers = new HashSet<>();
//		for(Object eClass : allClasses) {
//			if(eClass instanceof EClass) {
//				EClass classifier = (EClass) eClass;
//				if (classifier.getESuperTypes().contains(superClassifier)) {
//					extendingClassifiers.add(classifier);
//					extendingClassifiers.addAll(getExtendingClassifiers((EClass) eClass));
//				}
//			}
//		}
//	
//		return extendingClassifiers;
//	}

	private boolean isGrayed(Object object) {
		for (Object checkedElement : viewer.getGrayedElements()) {
			if (object.equals(checkedElement))
				return true;
		}
		return false;
	}

	private boolean isChecked(Object object) {
		for (Object checkedElement : viewer.getCheckedElements()) {
			if (object.equals(checkedElement))
				return true;
		}
		return false;
	}

}
