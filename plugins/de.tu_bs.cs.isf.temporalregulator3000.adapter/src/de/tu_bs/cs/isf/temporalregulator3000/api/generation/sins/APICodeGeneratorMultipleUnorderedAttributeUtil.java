package de.tu_bs.cs.isf.temporalregulator3000.api.generation.sins;

import de.tu_bs.cs.isf.temporalregulator3000.api.generation.sins.util.APICodeGeneratorInfoWrapper;

public class APICodeGeneratorMultipleUnorderedAttributeUtil {
	
	public static String createContentsForGetMultipleUnorderedAttributeMethod(APICodeGeneratorInfoWrapper info) {
		return
			"	public List<" + info.nonTemporalType + "> get" + info.featureName + "At(" + info.nonTemporalAdapterInterface + " adapter, Date timePoint) {" + System.lineSeparator() + 
			"		EObject temporalMetaModelElement = getTemporalObjectFromCacheMap(adapter);" + System.lineSeparator() + 
			"		" + System.lineSeparator() + 
			"		if(temporalMetaModelElement instanceof " + info.temporalAdapterInterface + ") {" + System.lineSeparator() + 
			"			List<" + info.associationReplacementInterfaceName + "> temporalAttributes = ((" + info.temporalAdapterInterface + ") temporalMetaModelElement).getTemporal" + info.featureName + "Replacement();" + System.lineSeparator() + 
			"			List<" + info.associationReplacementInterfaceName + "> validTemporalAttributes = EvolutionUtil.getValidTemporalElements(temporalAttributes, timePoint);" + System.lineSeparator() + 
			"			" + System.lineSeparator() + 
			"			List<" + info.nonTemporalType + "> resultList = new LinkedList<>();" + System.lineSeparator() + 
			"			for(" + info.associationReplacementInterfaceName + " validTemporalAttribute : validTemporalAttributes) {" + System.lineSeparator() + 
			"				resultList.add(validTemporalAttribute.get" + info.featureName + "());" + System.lineSeparator() + 
			"			}" + System.lineSeparator() + 
			"			return resultList;" + System.lineSeparator() + 
			"		}" + System.lineSeparator() + 
			"		" + System.lineSeparator() + 
			"		throw new UnsupportedOperationException(\"operation failed.\");" + System.lineSeparator() + 
			"	}";
	}
	
	public static String createContentsForAddMultipleUnorderedAttributeMethod(APICodeGeneratorInfoWrapper info) {
		return 
			"	public void add" + info.featureName + "At(" + info.nonTemporalAdapterInterface + " adapter, List<Object> elements, int listIndexOfOperation, Date timePoint) {" + System.lineSeparator() + 
			"		Collections.reverse(elements);" + System.lineSeparator() + 
			"		EObject temporalMetaModelElement = getTemporalObjectFromCacheMap(adapter);" + System.lineSeparator() + 
			"		" + System.lineSeparator() + 
			"		if(temporalMetaModelElement instanceof " + info.temporalAdapterInterface + ") {" + System.lineSeparator() + 
			"			List<" + info.associationReplacementInterfaceName + "> temporalAttributes = ((" + info.temporalAdapterInterface + ") temporalMetaModelElement).getTemporal" + info.featureName + "Replacement();" + System.lineSeparator() + 
			"			" + System.lineSeparator() + 
			"			for(Object attributeToAdd : elements) {" + System.lineSeparator() + 
			"				Assert.isNotNull(attributeToAdd);" + System.lineSeparator() + 
			"				if(attributeToAdd instanceof " + info.nonTemporalType + ") {" + System.lineSeparator() + 
			"					" + info.associationReplacementInterfaceName + " newAttribute = " + info.temporalFactory + ".eINSTANCE.create" + info.associationReplacementInterfaceName + "();" + System.lineSeparator() + 
			"					newAttribute.setTemporalRegulatorValidSince(timePoint);" + System.lineSeparator() + 
			"					newAttribute.set" + info.featureName + "((" + info.nonTemporalType + ") attributeToAdd);" + System.lineSeparator() + 
			"					" + System.lineSeparator() + 
			"					temporalAttributes.add(newAttribute);" + System.lineSeparator() + 
			"				}" + System.lineSeparator() + 
			"				else throw new RuntimeException(\"Wrong type of attribute!\");" + System.lineSeparator() + 
			"			}" + System.lineSeparator() + 
			"			" + System.lineSeparator() + 
			"			return;" + System.lineSeparator() + 
			"		}" + System.lineSeparator() + 
			"		" + System.lineSeparator() + 
			"		throw new UnsupportedOperationException(\"operation failed.\");" + System.lineSeparator() + 
			"	}";
	}
	
	public static String createContentsForRemoveMultipleUnorderedAttributeMethod(APICodeGeneratorInfoWrapper info) {
		return
			"	public void remove" + info.featureName + "At(" + info.nonTemporalAdapterInterface + " adapter, List<Object> elements, Date timePoint) {" + System.lineSeparator() + 
			"		EObject temporalMetaModelElement = getTemporalObjectFromCacheMap(adapter);" + System.lineSeparator() + 
			"		" + System.lineSeparator() + 
			"		if(temporalMetaModelElement instanceof " + info.temporalAdapterInterface + ") {" + System.lineSeparator() + 
			"			List<" + info.associationReplacementInterfaceName + "> temporalAttributes = ((" + info.temporalAdapterInterface + ") temporalMetaModelElement).getTemporal" + info.featureName + "Replacement();" + System.lineSeparator() + 
			"			List<" + info.associationReplacementInterfaceName + "> validTemporalAttributes = EvolutionUtil.getValidTemporalElements(temporalAttributes, timePoint);" + System.lineSeparator() + 
			"			" + System.lineSeparator() + 
			"			for(" + info.associationReplacementInterfaceName + " validTemporalAttribute : validTemporalAttributes) {" + System.lineSeparator() + 
			"				for(Object attributeToRemove : elements) {" + System.lineSeparator() + 
			"					Assert.isNotNull(attributeToRemove);" + System.lineSeparator() + 
			"					if(validTemporalAttribute.get" + info.featureName + "().equals(attributeToRemove)) {" + System.lineSeparator() + 
			"						validTemporalAttribute.setTemporalRegulatorValidUntil(timePoint);" + System.lineSeparator() + 
			"						TemporalUtil.doAdditionalTemporalCorrections(temporalMetaModelElement, temporalAttributes, validTemporalAttribute, null, timePoint);" + System.lineSeparator() + 
			"					}" + System.lineSeparator() + 
			"				}" + System.lineSeparator() + 
			"			}" + System.lineSeparator() + 
			"			" + System.lineSeparator() + 
			"			return;" + System.lineSeparator() + 
			"		}" + System.lineSeparator() + 
			"		" + System.lineSeparator() + 
			"		throw new UnsupportedOperationException(\"operation failed.\");" + System.lineSeparator() + 
			"	}";
	}
	
}
