package de.tu_bs.cs.isf.temporalregulator3000.api.generation;

import org.eclipse.emf.codegen.ecore.genmodel.GenPackage;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;

public abstract class BaseCodeGenerator {
	
	protected IPackageFragment javaPackage;
	protected IType javaClass;
	protected GenPackage temporalGenPackage, nonTemporalGenPackage;
	protected String temporalRootPackage, nonTemporalRootPackage;
	
	
	
	public BaseCodeGenerator(IPackageFragment javaPackage, GenPackage temporalGenPackage, GenPackage nonTemporalGenPackage) {
		this.javaPackage = javaPackage;

		if(temporalGenPackage != null) {
			this.temporalGenPackage = temporalGenPackage;
			this.temporalRootPackage = getRootPackageName(temporalGenPackage);
		}
		if(nonTemporalGenPackage != null) {
			this.nonTemporalGenPackage = nonTemporalGenPackage;
			this.nonTemporalRootPackage = getRootPackageName(nonTemporalGenPackage);
		}
	}
	
	
	
	protected void generateSingletonCode(String contents) throws JavaModelException {
		String className = javaClass.getElementName();
		
		javaClass.createField("public static " + className + " instance = new " + className + "();", null, false, null);
		javaClass.createMethod("private " + className + "() {" + contents + "}", null, false, null);
	}
	
	
	
	protected void generateNewMethodCode(String contents) throws JavaModelException {
		if(javaClass == null)
			throw new UnsupportedOperationException("A method tried to write into a class before it was created:\n\n" + contents);
		
		javaClass.createMethod(contents + System.lineSeparator(), null, false, null);
	}
	
	

	private String getRootPackageName(GenPackage genPackage) {
		return genPackage.getBasePackage() + "." + genPackage.getPackageName();
	}

}
