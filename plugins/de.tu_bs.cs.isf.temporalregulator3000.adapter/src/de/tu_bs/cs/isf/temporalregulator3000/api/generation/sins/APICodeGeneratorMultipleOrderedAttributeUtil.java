package de.tu_bs.cs.isf.temporalregulator3000.api.generation.sins;

import de.tu_bs.cs.isf.temporalregulator3000.api.generation.sins.util.APICodeGeneratorInfoWrapper;

public class APICodeGeneratorMultipleOrderedAttributeUtil {
	
	public static String createContentsForGetMultipleOrderedAttributeMethod(APICodeGeneratorInfoWrapper info) {
		String utilInstanceName = "utilFor" + info.camelCasedQualifiedFeatureName;
		
		return
				"	public List<" + info.nonTemporalType + "> get" + info.featureName + "At(" + info.nonTemporalAdapterInterface + " adapter, Date timePoint) {" + System.lineSeparator() + 
				"		EObject temporalMetaModelElement = getTemporalObjectFromCacheMap(adapter);" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		if(temporalMetaModelElement instanceof " + info.temporalAdapterInterface + ") {" + System.lineSeparator() + 
				"			TemporallySortedList<" + info.nonTemporalType + "> attributeList = ((" + info.temporalAdapterInterface + ") temporalMetaModelElement).getTemporal" + info.featureName + "Replacement();" + System.lineSeparator() + 
				"			if(attributeList == null) {" + System.lineSeparator() + 
				"				attributeList = " + utilInstanceName + ".createTemporallySortedList();" + System.lineSeparator() + 
				"				((" + info.temporalAdapterInterface + ") temporalMetaModelElement).setTemporal" + info.featureName + "Replacement(attributeList);" + System.lineSeparator() + 
				"			}" + System.lineSeparator() + 
				"			" + System.lineSeparator() + 
				"			return " + utilInstanceName + ".getSortedElementsAt(attributeList, timePoint);" + System.lineSeparator() + 
				"		}" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		throw new UnsupportedOperationException(\"operation failed.\");" + System.lineSeparator() + 
				"	}";
	}
	
	
	
	
	
	public static String createContentsForAddMultipleOrderedAttributeMethod(APICodeGeneratorInfoWrapper info) {
		String utilInstanceName = "utilFor" + info.camelCasedQualifiedFeatureName;
		
		return 
				"	public void add" + info.featureName + "At(" + info.nonTemporalAdapterInterface + " adapter, List<Object> elements, int listIndexOfOperation, Date timePoint) {" + System.lineSeparator() + 
				"		Collections.reverse(elements);" + System.lineSeparator() + 
				"		EObject temporalMetaModelElement = getTemporalObjectFromCacheMap(adapter);" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		if(temporalMetaModelElement instanceof " + info.temporalAdapterInterface + ") {" + System.lineSeparator() + 
				"			TemporallySortedList<" + info.nonTemporalType + "> attributeList = ((" + info.temporalAdapterInterface + ") temporalMetaModelElement).getTemporal" + info.featureName + "Replacement();" + System.lineSeparator() + 
				"			" + System.lineSeparator() + 
				"			for(Object attributeToAdd : elements) {" + System.lineSeparator() + 
				"				Assert.isNotNull(attributeToAdd);" + System.lineSeparator() + 
				"				if(attributeToAdd instanceof " + info.nonTemporalType + ") {" + System.lineSeparator() + 
				"					if(listIndexOfOperation != -1)" + System.lineSeparator() + 
				"						" + utilInstanceName + ".addElementAt(attributeList, (" + info.nonTemporalType + ") attributeToAdd, listIndexOfOperation, timePoint);" + System.lineSeparator() + 
				"					else" + System.lineSeparator() + 
				"						" + utilInstanceName + ".addElementAt(attributeList, (" + info.nonTemporalType + ") attributeToAdd, timePoint);" + System.lineSeparator() + 
				"				} else {" + System.lineSeparator() + 
				"					throw new RuntimeException(\"Wrong type of attribute!\");" + System.lineSeparator() + 
				"				}" + System.lineSeparator() + 
				"			}" + System.lineSeparator() + 
				"			" + System.lineSeparator() + 
				"			return;" + System.lineSeparator() + 
				"		}" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		throw new UnsupportedOperationException(\"operation failed.\");" + System.lineSeparator() + 
				"	}";
	}
	
	
	
	
	
	public static String createContentsForRemoveMultipleOrderedAttributeMethod(APICodeGeneratorInfoWrapper info) {
		String utilInstanceName = "utilFor" + info.camelCasedQualifiedFeatureName;
		
		return
				"	public void remove" + info.featureName + "At(" + info.nonTemporalAdapterInterface + " adapter, List<Object> elements, Date timePoint) {" + System.lineSeparator() + 
				"		EObject temporalMetaModelElement = getTemporalObjectFromCacheMap(adapter);" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		if(temporalMetaModelElement instanceof " + info.temporalAdapterInterface + ") {" + System.lineSeparator() + 
				"			TemporallySortedList<" + info.nonTemporalType + "> attributeList = ((" + info.temporalAdapterInterface + ") temporalMetaModelElement).getTemporal" + info.featureName + "Replacement();" + System.lineSeparator() + 
				"			" + System.lineSeparator() + 
				"			for(Object attributeToRemove : elements) {" + System.lineSeparator() + 
				"				Assert.isNotNull(attributeToRemove);" + System.lineSeparator() + 
				"				if(attributeToRemove instanceof " + info.nonTemporalType + ")" + System.lineSeparator() + 
				"					" + utilInstanceName + ".removeElementAt(attributeList, (" + info.nonTemporalType + ") attributeToRemove, timePoint);" + System.lineSeparator() + 
				"				else" + System.lineSeparator() + 
				"					throw new RuntimeException(\"Wrong type of attribute!\");" + System.lineSeparator() + 
				"			}" + System.lineSeparator() + 
				"			" + System.lineSeparator() + 
				"			return;" + System.lineSeparator() + 
				"		}" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		throw new UnsupportedOperationException(\"operation failed.\");" + System.lineSeparator() + 
				"	}";
	}
	
}
