package de.tu_bs.cs.isf.temporalregulator3000.api.generation;

import org.eclipse.emf.codegen.ecore.genmodel.GenPackage;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.JavaModelException;

import de.tu_bs.cs.isf.temporalregulator3000.api.CodeGenerator;

public class FacadeCodeGenerator extends BaseCodeGenerator {
	
	public String FACADE_NAME;
	
	
	
	public FacadeCodeGenerator(IPackageFragment javaPackage, GenPackage temporalGenPackage, GenPackage nonTemporalGenPackage) {
		super(javaPackage, temporalGenPackage, nonTemporalGenPackage);
	}
	
	

	public void generateBaseCode() throws JavaModelException {
		FACADE_NAME = CodeGenerator.instance.getNamePrefix() + "TemporalAwareFacade";
		
		ICompilationUnit cu = javaPackage.createCompilationUnit(FACADE_NAME + ".java", "", true, null);
		CodeGenerator.instance.registerGeneratedSourceFile(cu.getCorrespondingResource());
		
		cu.createPackageDeclaration(javaPackage.getElementName(), null);
		cu.createImport("java.util.Date", null, null);
		cu.createImport("java.util.List", null, null);
		cu.createImport("org.eclipse.emf.ecore.EObject", null, null);
		cu.createImport("de.tu_bs.cs.isf.temporalregulator3000.core.ClockMechanism", null, null);
		cu.createImport("de.tu_bs.cs.isf.temporalregulator3000.core.RegulatorDecoratorEList", null, null);
		
		javaClass = cu.createType("public class " + FACADE_NAME + " {" + System.lineSeparator() + System.lineSeparator() + "}", null, false, null);
		generateSingletonCode("");
		
		javaClass.createMethod(
				"	private Date getTime() {" + System.lineSeparator() + 
				"		return ClockMechanism.instance.getTime();" + System.lineSeparator() + 
				"	}"
				, null, false, null);
		
		javaClass.createMethod(
				"	public void deleteElement(EObject element) {" + System.lineSeparator() + 
				"		" + CodeGenerator.instance.getQualifiedGatewayName() + ".instance.deleteElementAt(element, getTime());" + System.lineSeparator() + 
				"	}"
				, null, false, null);
	}
	
	

	public void generateTemporalAwareAdapterGetterCode(String type, String keyword, String name, String adapterInterface) throws JavaModelException {
		String contents =
				"	public " + type + " " + keyword + name + "(" + adapterInterface + " adapter) {" + System.lineSeparator() + 
				"		return " + CodeGenerator.instance.getQualifiedGatewayName() + ".instance." + keyword + name + "At(adapter, getTime());" + System.lineSeparator() + 
				"	}";
		generateNewMethodCode(contents);
	}
	
	
	
	public void generateTemporalAwareAdapterUpdateCode(String type, String keyword, String name, String adapterInterface) throws JavaModelException {
		String s1 = "", s2 = "";
		if(keyword.equals("add")) {
			s1 = ", int listIndex";
			s2 = ", listIndex";
		}
		String contents =
				"	public void " + keyword + name + "(" + adapterInterface + " adapter, " + type + " value" + s1 + ") {" + System.lineSeparator() + 
				"		" + CodeGenerator.instance.getQualifiedGatewayName() + ".instance." + keyword + name + "At(adapter, value" + s2 + ", getTime());" + System.lineSeparator() + 
				"	}";
		
		generateNewMethodCode(contents);
	}
	
}
