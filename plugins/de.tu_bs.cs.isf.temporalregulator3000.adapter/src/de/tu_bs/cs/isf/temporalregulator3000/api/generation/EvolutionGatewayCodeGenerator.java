package de.tu_bs.cs.isf.temporalregulator3000.api.generation;

import static de.tu_bs.cs.isf.temporalregulator3000.api.generation.sins.APICodeGeneratorMultipleOrderedAttributeUtil.createContentsForAddMultipleOrderedAttributeMethod;
import static de.tu_bs.cs.isf.temporalregulator3000.api.generation.sins.APICodeGeneratorMultipleOrderedAttributeUtil.createContentsForGetMultipleOrderedAttributeMethod;
import static de.tu_bs.cs.isf.temporalregulator3000.api.generation.sins.APICodeGeneratorMultipleOrderedAttributeUtil.createContentsForRemoveMultipleOrderedAttributeMethod;
import static de.tu_bs.cs.isf.temporalregulator3000.api.generation.sins.APICodeGeneratorMultipleOrderedReferenceUtil.createContentsForAddMultipleOrderedReferenceMethod;
import static de.tu_bs.cs.isf.temporalregulator3000.api.generation.sins.APICodeGeneratorMultipleOrderedReferenceUtil.createContentsForGetMultipleOrderedReferenceMethod;
import static de.tu_bs.cs.isf.temporalregulator3000.api.generation.sins.APICodeGeneratorMultipleOrderedReferenceUtil.createContentsForRemoveMultipleOrderedReferenceMethod;
import static de.tu_bs.cs.isf.temporalregulator3000.api.generation.sins.APICodeGeneratorMultipleUnorderedAttributeUtil.createContentsForAddMultipleUnorderedAttributeMethod;
import static de.tu_bs.cs.isf.temporalregulator3000.api.generation.sins.APICodeGeneratorMultipleUnorderedAttributeUtil.createContentsForGetMultipleUnorderedAttributeMethod;
import static de.tu_bs.cs.isf.temporalregulator3000.api.generation.sins.APICodeGeneratorMultipleUnorderedAttributeUtil.createContentsForRemoveMultipleUnorderedAttributeMethod;
import static de.tu_bs.cs.isf.temporalregulator3000.api.generation.sins.APICodeGeneratorMultipleUnorderedReferenceUtil.createContentsForAddMultipleUnorderedReferenceMethod;
import static de.tu_bs.cs.isf.temporalregulator3000.api.generation.sins.APICodeGeneratorMultipleUnorderedReferenceUtil.createContentsForGetMultipleUnorderedReferenceMethod;
import static de.tu_bs.cs.isf.temporalregulator3000.api.generation.sins.APICodeGeneratorMultipleUnorderedReferenceUtil.createContentsForRemoveMultipleUnorderedReferenceMethod;
import static de.tu_bs.cs.isf.temporalregulator3000.api.generation.sins.APICodeGeneratorSingleAttributeUtil.createContentsForGetSingleAttributeMethod;
import static de.tu_bs.cs.isf.temporalregulator3000.api.generation.sins.APICodeGeneratorSingleAttributeUtil.createContentsForSetSingleAttributeMethod;
import static de.tu_bs.cs.isf.temporalregulator3000.api.generation.sins.APICodeGeneratorSingleReferenceUtil.createContentsForGetSingleReferenceMethod;
import static de.tu_bs.cs.isf.temporalregulator3000.api.generation.sins.APICodeGeneratorSingleReferenceUtil.createContentsForSetSingleReferenceMethod;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.codegen.ecore.genmodel.GenPackage;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.JavaModelException;

import de.tu_bs.cs.isf.temporalregulator3000.api.CodeGenerator;
import de.tu_bs.cs.isf.temporalregulator3000.api.generation.sins.util.APICodeGeneratorInfoWrapper;

public class EvolutionGatewayCodeGenerator extends BaseCodeGenerator {
	
	public String GATEWAY_NAME;
	
	
	
	public EvolutionGatewayCodeGenerator(IPackageFragment javaPackage, GenPackage temporalGenPackage, GenPackage nonTemporalGenPackage) {
		super(javaPackage, temporalGenPackage, nonTemporalGenPackage);
	}
	
	

	public void generateBaseCode() throws CoreException {
		GATEWAY_NAME = CodeGenerator.instance.getNamePrefix() + "EvolutionGateway";
		
		ICompilationUnit cu = javaPackage.createCompilationUnit(GATEWAY_NAME + ".java", "", true, null);
		CodeGenerator.instance.registerGeneratedSourceFile(cu.getCorrespondingResource());
		
		cu.createPackageDeclaration(javaPackage.getElementName(), null);
		
		cu.createImport("org.eclipse.core.runtime.Assert", null, null);
		
		cu.createImport("java.util.Collections", null, null);
		cu.createImport("java.util.Date", null, null);
		cu.createImport("java.util.LinkedList", null, null);
		cu.createImport("java.util.List", null, null);

		cu.createImport("org.eclipse.emf.ecore.EObject", null, null);
		cu.createImport("org.eclipse.emf.ecore.util.EcoreUtil", null, null);
		
		cu.createImport("com.google.common.collect.BiMap", null, null);
		cu.createImport("com.google.common.collect.HashBiMap", null, null);
		
		cu.createImport("de.tu_bs.cs.isf.temporalregulator3000.core.TemporalUtil", null, null);
		cu.createImport("de.tu_bs.cs.isf.temporalregulator3000.core.TemporallySortedListUtil", null, null);
		cu.createImport("de.tu_bs.cs.isf.temporalregulator3000.model.util.EvolutionUtil", null, null);
		cu.createImport("de.tu_bs.cs.isf.temporalregulator3000.temporalmodel.TemporalElement", null, null);
		cu.createImport("de.tu_bs.cs.isf.temporalregulator3000.temporalmodel.TemporallySortedList", null, null);
		
		javaClass = cu.createType("public class " + GATEWAY_NAME + " {" + System.lineSeparator() + System.lineSeparator() + "}", null, false, null);

		javaClass.createField(
				"	// This map stores all instances of adapter elements that were created by the custom factory along with their non-temporal counterpart" + System.lineSeparator() + 
				"	private BiMap<EObject, EObject> classInstanceCacheMap;"
				, null, false, null);
		
		generateSingletonCode(System.lineSeparator() + "		this.classInstanceCacheMap = HashBiMap.create();" + System.lineSeparator());

		javaClass.createMethod(
				"	public void addCacheMapEntry(EObject commonMetaModelElement, EObject temporalMetaModelElement) {" + System.lineSeparator() + 
				"		try {" + System.lineSeparator() + 
				"			this.classInstanceCacheMap.put(commonMetaModelElement, temporalMetaModelElement);" + System.lineSeparator() + 
				"		} catch(IllegalArgumentException e) {" + System.lineSeparator() + 
				"			// NO DOUBLE ENTRIES! Neither in keys or values!" + System.lineSeparator() + 
				"			throw e;" + System.lineSeparator() + 
				"		}" + System.lineSeparator() + 
				"	}"
				, null, false, null);

		javaClass.createMethod(
				"	public EObject getTemporalObjectFromCacheMap(EObject commonMetaModelElement) {" + System.lineSeparator() + 
				"		EObject temporalObject = this.classInstanceCacheMap.get(commonMetaModelElement);" + System.lineSeparator() + 
				"		if(temporalObject == null)" + System.lineSeparator() + 
				"			return commonMetaModelElement;" + System.lineSeparator() + 
				"		return temporalObject;" + System.lineSeparator() + 
				"	}"
				, null, false, null);

		javaClass.createMethod(
				"	public boolean deleteElementAt(EObject commonMetaModelElement, Date timePoint) {" + System.lineSeparator() + 
				"		EObject uncastedTemporalElement = this.getTemporalObjectFromCacheMap(commonMetaModelElement);" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		if(uncastedTemporalElement instanceof TemporalElement) {" + System.lineSeparator() + 
				"			TemporalElement temporalElement = (TemporalElement) uncastedTemporalElement;" + System.lineSeparator() + 
				"			temporalElement.setTemporalRegulatorValidUntil(timePoint);" + System.lineSeparator() + 
				"			" + System.lineSeparator() + 
				"			if(temporalElement.getTemporalRegulatorValidSince().compareTo(temporalElement.getTemporalRegulatorValidUntil()) >= 0)" + System.lineSeparator() + 
				"				EcoreUtil.delete(temporalElement);" + System.lineSeparator() + 
				"			return true;" + System.lineSeparator() + 
				"		}" + System.lineSeparator() + 
				"		else {" + System.lineSeparator() + 
				"			// TODO" + System.lineSeparator() + 
				"		}" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		return false;" + System.lineSeparator() + 
				"	}"
				, null, false, null);
	}
	


	public void addImport(String newImport) throws JavaModelException {
		javaClass.getCompilationUnit().createImport(newImport,  null,  null);
	}
	
	private Map<String, String> temporalInterfaceNamesForInstanceCheck = new HashMap<>();
	
	public void addInstanceCheckForNonTemporalCacheMapGetter(String temporalInterfaceName, String temporalPackage) {
		temporalInterfaceNamesForInstanceCheck.put(temporalInterfaceName, temporalPackage);
	}
	
	public void generateGetNonTemporalObjectFromCacheMapMethod() throws JavaModelException {
		String pre =
				"	public EObject getNonTemporalObjectFromCacheMap(EObject temporalElement) {" + System.lineSeparator() + 
				"		EObject cachedObject = this.classInstanceCacheMap.inverse().get(temporalElement);" + System.lineSeparator() + 
				"		if (cachedObject == null) {" + System.lineSeparator();
		
		String var = "";
		for(String interfaceName : temporalInterfaceNamesForInstanceCheck.keySet()) {
			String packageName = temporalInterfaceNamesForInstanceCheck.get(interfaceName);
			var +=	"			if(temporalElement instanceof " + packageName + "." + interfaceName + ")" + System.lineSeparator() + 
					"				addCacheMapEntry(new " + packageName + ".impl.TemporalAwareAdapterFor" + interfaceName + "Impl(), temporalElement);" + System.lineSeparator() + 
					"			" + System.lineSeparator();
		}
		
		String post =
				"			cachedObject = this.classInstanceCacheMap.inverse().get(temporalElement);" + System.lineSeparator() + 
				"		}" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		if (cachedObject == null) {" + System.lineSeparator() + 
				"			// \"temporalElement\" is probably a non-temporal element..." + System.lineSeparator() + 
				"			return temporalElement;" + System.lineSeparator() + 
				"		}" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		return cachedObject;" + System.lineSeparator() + 
				"	}";
		
		javaClass.createMethod(pre + var + post, null, false, null);
	}
	
	
	
	// Single Features
	
	
	
	public void generateGetSingleAttributeMethodCode(APICodeGeneratorInfoWrapper info) throws JavaModelException {
		generateNewMethodCode(createContentsForGetSingleAttributeMethod(info));
	}
	
	public void generateSetSingleAttributeMethodCode(APICodeGeneratorInfoWrapper info) throws JavaModelException {
		generateNewMethodCode(createContentsForSetSingleAttributeMethod(info));
	}
	
	
	
	public void generateGetSingleReferenceMethodCode(APICodeGeneratorInfoWrapper info) throws JavaModelException {
		generateNewMethodCode(createContentsForGetSingleReferenceMethod(info));
	}
	
	public void generateSetSingleReferenceMethodCode(APICodeGeneratorInfoWrapper info) throws JavaModelException {
		if(info.hasOppositeReference && info.isTemporalFeature)
			generateNewMethodCode(createContentsForSetSingleReferenceProxyMethod(info));
		
		generateNewMethodCode(createContentsForSetSingleReferenceMethod(info));
	}
	
	
	
	// Multiple Unordered Features
	
	
	
	public void generateGetMutlipleUnorderedAttributeMethodCode(APICodeGeneratorInfoWrapper info) throws JavaModelException {
		generateNewMethodCode(createContentsForGetMultipleUnorderedAttributeMethod(info));
	}
	
	public void generateAddMutlipleUnorderedAttributeMethodCode(APICodeGeneratorInfoWrapper info) throws JavaModelException {
		generateNewMethodCode(createContentsForAddMultipleUnorderedAttributeMethod(info));
	}
	
	public void generateRemoveMutlipleUnorderedAttributeMethodCode(APICodeGeneratorInfoWrapper info) throws JavaModelException {
		generateNewMethodCode(createContentsForRemoveMultipleUnorderedAttributeMethod(info));
	}
	
	
	
	public void generateGetMutlipleUnorderedReferenceMethodCode(APICodeGeneratorInfoWrapper info) throws JavaModelException {
		generateNewMethodCode(createContentsForGetMultipleUnorderedReferenceMethod(info));
	}
	
	public void generateAddMutlipleUnorderedReferenceMethodCode(APICodeGeneratorInfoWrapper info) throws JavaModelException {
		if(info.hasOppositeReference && info.isTemporalFeature)
			generateNewMethodCode(createContentsForRemoveMultipleReferenceProxyMethod(info));
		
		generateNewMethodCode(createContentsForAddMultipleUnorderedReferenceMethod(info));
	}
	
	public void generateRemoveMutlipleUnorderedReferenceMethodCode(APICodeGeneratorInfoWrapper info) throws JavaModelException {
		if(info.hasOppositeReference && info.isTemporalFeature)
			generateNewMethodCode(createContentsForAddMultipleReferenceProxyMethod(info));
		
		generateNewMethodCode(createContentsForRemoveMultipleUnorderedReferenceMethod(info));
	}
	
	
	
	// Multiple Ordered Features
	
	
	
	private Set<String> addedUtilFields = new HashSet<>();
	
	public void generateUtilFieldCodeForOrderedFeature(String camelCasedQualifiedFeatureName, String valueType) throws JavaModelException {
		if(addedUtilFields.contains(camelCasedQualifiedFeatureName))
			return;
		
		String contents = "private TemporallySortedListUtil<" + valueType + "> " + camelCasedQualifiedFeatureName + " = new TemporallySortedListUtil<>();";
		javaClass.createField(contents + System.lineSeparator(), null, false, null);
		
		addedUtilFields.add(camelCasedQualifiedFeatureName);
	}
	
	
	
	public void generateGetMutlipleOrderedAttributeMethodCode(APICodeGeneratorInfoWrapper info) throws JavaModelException {
		generateNewMethodCode(createContentsForGetMultipleOrderedAttributeMethod(info));
	}
	
	public void generateAddMutlipleOrderedAttributeMethodCode(APICodeGeneratorInfoWrapper info) throws JavaModelException {
		generateNewMethodCode(createContentsForAddMultipleOrderedAttributeMethod(info));
	}
	
	public void generateRemoveMutlipleOrderedAttributeMethodCode(APICodeGeneratorInfoWrapper info) throws JavaModelException {
		generateNewMethodCode(createContentsForRemoveMultipleOrderedAttributeMethod(info));
	}
	
	
	
	public void generateGetMutlipleOrderedReferenceMethodCode(APICodeGeneratorInfoWrapper info) throws JavaModelException {
		generateNewMethodCode(createContentsForGetMultipleOrderedReferenceMethod(info));
	}
	
	public void generateAddMutlipleOrderedReferenceMethodCode(APICodeGeneratorInfoWrapper info) throws JavaModelException {
		if(info.hasOppositeReference && info.isTemporalFeature)
			generateNewMethodCode(createContentsForAddMultipleReferenceProxyMethod(info));
		
		generateNewMethodCode(createContentsForAddMultipleOrderedReferenceMethod(info));
	}
	
	public void generateRemoveMutlipleOrderedReferenceMethodCode(APICodeGeneratorInfoWrapper info) throws JavaModelException {
		if(info.hasOppositeReference && info.isTemporalFeature)
			generateNewMethodCode(createContentsForRemoveMultipleReferenceProxyMethod(info));
		
		generateNewMethodCode(createContentsForRemoveMultipleOrderedReferenceMethod(info));
	}
	
	
	
	
	
	
	

	
	public String createContentsForSetSingleReferenceProxyMethod(APICodeGeneratorInfoWrapper info) {
		return "	public void set" + info.featureName + "At(" + info.nonTemporalAdapterInterface + " adapter, " + info.nonTemporalType + " commonValue, Date timePoint) {" + System.lineSeparator() + 
				"		set" + info.featureName + "At(adapter, commonValue, timePoint, true);" + 
				"	}";
	}
	
	public String createContentsForAddMultipleReferenceProxyMethod(APICodeGeneratorInfoWrapper info) {
		return "	public void add" + info.featureName + "At(" + info.nonTemporalAdapterInterface + " adapter, List<Object> elements, int listIndexOfOperation, Date timePoint) {" + System.lineSeparator() + 
				"		add" + info.featureName + "At(adapter, elements, listIndexOfOperation, timePoint, true);" + 
				"	}";
	}
	
	public String createContentsForRemoveMultipleReferenceProxyMethod(APICodeGeneratorInfoWrapper info) {
		return "	public void remove" + info.featureName + "At(" + info.nonTemporalAdapterInterface + " adapter, List<Object> elements, Date timePoint) {" + System.lineSeparator() + 
				"		remove" + info.featureName + "At(adapter, elements, timePoint, true);" + 
				"	}";
	}
	
	
	
	
	
	
	
	
	
	
	
	public void generateMethodForGettingSingleOrMultipleNonTemporalFeature(APICodeGeneratorInfoWrapper info) throws JavaModelException {
		this.generateNewMethodCode(
				"	public " + info.nonTemporalType + " " + info.obtainingMethodPrefix + info.featureName + "At(" + info.nonTemporalAdapterInterface + " adapter, Date timePoint) {" + System.lineSeparator() + 
				"		EObject temporalMetaModelElement = classInstanceCacheMap.get(adapter);" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		if(temporalMetaModelElement instanceof " + info.temporalAdapterInterface + ")" + System.lineSeparator() + 
				"			return ((" + info.temporalAdapterInterface + ") temporalMetaModelElement)." + info.obtainingMethodPrefix + info.featureName + "();" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		throw new UnsupportedOperationException(\"operation failed.\");" + System.lineSeparator() + 
				"	}");
	}
	
	public void generateMethodForSettingNonTemporalSingleFeature(APICodeGeneratorInfoWrapper info) throws JavaModelException {
		this.generateNewMethodCode(
				"	public void set" + info.featureName + "At(" + info.nonTemporalAdapterInterface + " adapter, " + info.nonTemporalType + " value, Date timePoint) {" + System.lineSeparator() + 
				"		EObject temporalMetaModelElement = classInstanceCacheMap.get(adapter);" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		if(temporalMetaModelElement instanceof " + info.temporalAdapterInterface + ") {" + System.lineSeparator() + 
				"			((" + info.temporalAdapterInterface + ") temporalMetaModelElement).set" + info.featureName + "(value);" + System.lineSeparator() + 
				"			return;" + System.lineSeparator() + 
				"		}" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		throw new UnsupportedOperationException(\"operation failed.\");" + System.lineSeparator() + 
				"	}");
	}
	
	public void generateMethodForAddingNonTemporalMultipleFeature(APICodeGeneratorInfoWrapper info) throws JavaModelException {
		// TODO implement!
		this.generateNewMethodCode( 
				"	public void add" + info.featureName + "At(" + info.nonTemporalAdapterInterface + " adapter, List<Object> elements, int listIndexOfOperation, Date timePoint) {" + System.lineSeparator() + 
				"		// TODO" + System.lineSeparator() + 
				"		// TODO" + System.lineSeparator() + 
				"		// TODO" + System.lineSeparator() + 
				"		throw new UnsupportedOperationException(\"not implemented.\");" + System.lineSeparator() + 
				"	}");
	}
	
	public void generateMethodForRemovingNonTemporalMultipleFeature(APICodeGeneratorInfoWrapper info) throws JavaModelException {
		// TODO implement!
		this.generateNewMethodCode( 
				"	public void remove" + info.featureName + "At(" + info.nonTemporalAdapterInterface + " adapter, List<Object> elements, Date timePoint) {" + System.lineSeparator() + 
				"		// TODO" + System.lineSeparator() + 
				"		// TODO" + System.lineSeparator() + 
				"		// TODO" + System.lineSeparator() + 
				"		throw new UnsupportedOperationException(\"not implemented.\");" + System.lineSeparator() + 
				"	}");
	}
	
}














