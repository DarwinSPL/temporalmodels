package de.tu_bs.cs.isf.temporalregulator3000.api.generation.sins;

import de.tu_bs.cs.isf.temporalregulator3000.api.generation.sins.util.APICodeGeneratorInfoWrapper;
import de.tu_bs.cs.isf.temporalregulator3000.api.generation.sins.util.APICodeGeneratorOppositeHandlingUtil;

public class APICodeGeneratorSingleReferenceUtil {
	
	public static String createContentsForGetSingleReferenceMethod(APICodeGeneratorInfoWrapper info) {
		return
				"	public " + info.nonTemporalType + " get" + info.featureName + "At(" + info.nonTemporalAdapterInterface + " adapter, Date timePoint) {" + System.lineSeparator() + 
				"		EObject temporalMetaModelElement = getTemporalObjectFromCacheMap(adapter);" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		if(temporalMetaModelElement instanceof " + info.temporalAdapterInterface + ") {" + System.lineSeparator() + 
				"			List<" + info.associationReplacementInterfaceName + "> temporalReferences = ((" + info.temporalAdapterInterface + ") temporalMetaModelElement).getTemporal" + info.featureName + "Replacement();" + System.lineSeparator() + 
				"			" + info.associationReplacementInterfaceName + " validTemporalReference = EvolutionUtil.getValidTemporalElement(temporalReferences, timePoint);" + System.lineSeparator() + 
				"			" + System.lineSeparator() + 
				"			if(validTemporalReference != null) {" + System.lineSeparator() + 
				"				" + info.temporalType + " temporalReferenceTarget = validTemporalReference.get" + info.featureName + "();" + System.lineSeparator() + 
				"				return (" + info.nonTemporalType + ") getNonTemporalObjectFromCacheMap(temporalReferenceTarget);" + System.lineSeparator() + 
				"			}" + System.lineSeparator() + 
				"			" + System.lineSeparator() + 
				"			return null;" + System.lineSeparator() + 
				"		}" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		throw new UnsupportedOperationException(\"operation failed.\");" + System.lineSeparator() + 
				"	}";
	}
	
	
	
	public static String createContentsForSetSingleReferenceMethod(APICodeGeneratorInfoWrapper info) {
		String containmentCode = "";
		if(info.isContainment)
			containmentCode = System.lineSeparator() + 
			"				// Keep up the containment hierarchy" + System.lineSeparator() + 
			"				if(temporalValue.eContainer() == null)" + System.lineSeparator() + 
			"					((" + info.temporalAdapterInterface + ") temporalContainer).getRelocated" + info.associationReplacementInterfaceName + "().add((" + info.temporalType + ") temporalValue);" + System.lineSeparator();
		
		String oppositeReferenceMaintainCode = APICodeGeneratorOppositeHandlingUtil.createCodeForSetSingleReference(info);
		
		String headLine;
		if(!info.hasOppositeReference)
			headLine = "	public void set" + info.featureName + "At(" + info.nonTemporalAdapterInterface + " adapter, " + info.nonTemporalType + " commonValue, Date timePoint) {";
		else
			headLine = "	private void set" + info.featureName + "At(" + info.nonTemporalAdapterInterface + " adapter, " + info.nonTemporalType + " commonValue, Date timePoint, boolean maintainOpposite) {";
		
		return headLine + System.lineSeparator() + 
				"		EObject temporalContainer = getTemporalObjectFromCacheMap(adapter);" + System.lineSeparator() + 
				"		EObject temporalValue = getTemporalObjectFromCacheMap(commonValue);" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		" + oppositeReferenceMaintainCode + System.lineSeparator() + 
				"		if(temporalContainer instanceof " + info.temporalAdapterInterface + ") {" + System.lineSeparator() + 
				"			if(temporalValue instanceof " + info.temporalType + ") {" + System.lineSeparator() + 
				"				List<" + info.associationReplacementInterfaceName + "> temporalContainerReferences = ((" + info.temporalAdapterInterface + ") temporalContainer).getTemporal" + info.featureName + "Replacement();" + System.lineSeparator() + 
				"				" + info.associationReplacementInterfaceName + " oldTemporalContainerReference = EvolutionUtil.getValidTemporalElement(temporalContainerReferences, timePoint);" + System.lineSeparator() + 
				"				" + containmentCode + 
				"				" + System.lineSeparator() + 
				"				" + info.associationReplacementInterfaceName + " newTemporalContainerReference = " + info.temporalFactory + ".eINSTANCE.create" + info.associationReplacementInterfaceName + "();" + System.lineSeparator() + 
				"				newTemporalContainerReference.set" + info.featureName + "((" + info.temporalType + ") temporalValue);" + System.lineSeparator() + 
				"				newTemporalContainerReference.setTemporalRegulatorValidSince(timePoint);" + System.lineSeparator() + 
				"				temporalContainerReferences.add(newTemporalContainerReference);" + System.lineSeparator() + 
				"				" + System.lineSeparator() + 
				"				TemporalUtil.doAdditionalTemporalCorrections(temporalContainer, temporalContainerReferences, oldTemporalContainerReference, newTemporalContainerReference, timePoint);" + System.lineSeparator() + 
				"				return;" + System.lineSeparator() + 
				"			}" + System.lineSeparator() + 
				"			else if(temporalValue == null) {" + System.lineSeparator() + 
				"				// The user chose to set the reference to null" + System.lineSeparator() + 
				"				List<" + info.associationReplacementInterfaceName + "> temporalContainerReferences = ((" + info.temporalAdapterInterface + ") temporalContainer).getTemporal" + info.featureName + "Replacement();" + System.lineSeparator() + 
				"				" + info.associationReplacementInterfaceName + " validTemporalContainerReference = EvolutionUtil.getValidTemporalElement(temporalContainerReferences, timePoint);" + System.lineSeparator() + 
				"				validTemporalContainerReference.setTemporalRegulatorValidUntil(timePoint);" + System.lineSeparator() + 
				"				" + System.lineSeparator() + 
				"				TemporalUtil.doAdditionalTemporalCorrections(temporalContainer, temporalContainerReferences, validTemporalContainerReference, null, timePoint);" + System.lineSeparator() + 
				"				return;" + System.lineSeparator() + 
				"			}" + System.lineSeparator() + 
				"		}" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		throw new UnsupportedOperationException(\"operation failed.\");" + System.lineSeparator() + 
				"	}";
	}
	
}
