package de.tu_bs.cs.isf.temporalregulator3000.api.analysis;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.core.internal.utils.FileUtil;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.AbstractTypeDeclaration;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.EnumDeclaration;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.ImportDeclaration;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.SimpleType;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;

import de.tu_bs.cs.isf.temporalregulator3000.api.analysis.info.JavaFieldInformation;
import de.tu_bs.cs.isf.temporalregulator3000.api.analysis.info.JavaMethodInformation;
import de.tu_bs.cs.isf.temporalregulator3000.api.analysis.info.JavaPackageInformation;
import de.tu_bs.cs.isf.temporalregulator3000.api.analysis.info.JavaTypeInformation;
import de.tu_bs.cs.isf.temporalregulator3000.api.analysis.info.JavaTypeInformation.Type;
import de.tu_bs.cs.isf.temporalregulator3000.core.DialogUtil;

public class SourceFolderAnalyzer {
	
	private JavaPackageInformation currentPackage;
	private List<JavaPackageInformation> packageInformation;

	public List<JavaPackageInformation> analyse(IFolder src) {
		this.packageInformation = new ArrayList<>();
		
		try {
			for(IResource member : src.members()) {
				currentPackage = null;
				doAnalyse(member);
			}
		} catch (CoreException e) {
			e.printStackTrace();
		}
		
		return this.packageInformation;
	}
	
	
	
	
	
	private void doAnalyse(IResource res) throws CoreException {
		if(res instanceof IFile) {
			doAnalyseType((IFile) res);
		}
		else if(res instanceof IFolder) {
			doAnalysePackage((IFolder) res);
			
			for(IResource member : ((IFolder) res).members()) {
				doAnalyse(member);
			}
			
			this.currentPackage = this.currentPackage.getParentPackage();
		}
	}
	
	
	
	
	
	private void doAnalysePackage(IFolder folder) {
		if(this.currentPackage == null) {
			this.currentPackage = new JavaPackageInformation(folder.getName());
			packageInformation.add(this.currentPackage);
		}
		else {
			JavaPackageInformation newInfo = new JavaPackageInformation(folder.getName());
			this.currentPackage.addSubPackage(newInfo);
			this.currentPackage = newInfo;
		}
	}
	
	
	
	
	
	private void doAnalyseType(IFile file) {
		// Analyse the java file
		@SuppressWarnings("deprecation")
		ASTParser parser = ASTParser.newParser(AST.JLS8);
		parser.setSource(getFileContent(file));
		
		Map<String, String> options = JavaCore.getOptions();
		JavaCore.setComplianceOptions(JavaCore.VERSION_1_8, options);
		parser.setCompilerOptions(options);
		
		CompilationUnit compilationUnit = (CompilationUnit) parser.createAST(null);
		
		// Create info
		for(Object typeEntry : compilationUnit.types()) {
			AbstractTypeDeclaration type = (AbstractTypeDeclaration) typeEntry;
			JavaTypeInformation typeInfo = new JavaTypeInformation(type.getName().getIdentifier());
			
			for(Object importEntry : compilationUnit.imports()) {
				if(importEntry instanceof ImportDeclaration) {
					typeInfo.addImport(((ImportDeclaration) importEntry).getName().getFullyQualifiedName());
				}
			}
			
			if(typeEntry instanceof TypeDeclaration) {
				if(((TypeDeclaration) typeEntry).isInterface())
					setInterfaceInfos(typeInfo, (TypeDeclaration) typeEntry);
				else
					setClassInfos(typeInfo, (TypeDeclaration) typeEntry);
			}
			else if(typeEntry instanceof EnumDeclaration) {
				setEnumInfos(typeInfo, (EnumDeclaration) typeEntry);
			}
			
			this.currentPackage.addJavaType(typeInfo);
		}
	}
	
	
	
	
	
	private void setClassInfos(JavaTypeInformation typeInfo, TypeDeclaration typeDeclaration) {
		typeInfo.setType(Type.CLASS);
		
		addSuperInterfaces(typeInfo, typeDeclaration.superInterfaceTypes());
		
		org.eclipse.jdt.core.dom.Type superClass = typeDeclaration.getSuperclassType();
		if(superClass instanceof SimpleType) {
			typeInfo.setSuperType(((SimpleType) superClass).getName().getFullyQualifiedName());
		}
		
		addMethods(typeInfo, typeDeclaration.getMethods());
		addFields(typeInfo, typeDeclaration.getFields());
	}
	
	private void setInterfaceInfos(JavaTypeInformation typeInfo, TypeDeclaration typeDeclaration) {
		typeInfo.setType(Type.INTERFACE);

		addSuperInterfaces(typeInfo, typeDeclaration.superInterfaceTypes());

		addMethods(typeInfo, typeDeclaration.getMethods());
		addFields(typeInfo, typeDeclaration.getFields());
	}





	private void setEnumInfos(JavaTypeInformation typeInfo, EnumDeclaration typeDeclaration) {
		typeInfo.setType(Type.ENUM);

		addSuperInterfaces(typeInfo, typeDeclaration.superInterfaceTypes());
	}
	
	
	
	
	
	private void addSuperInterfaces(JavaTypeInformation typeInfo, List<?> superInterfacesTypes) {
		for(Object listEntry : superInterfacesTypes) {
			if(listEntry instanceof SimpleType) {
				typeInfo.addSuperInterface(((SimpleType) listEntry).getName().getFullyQualifiedName());
			}
		}
	}
	
	
	
	

	
	private void addFields(JavaTypeInformation typeInfo, FieldDeclaration[] fields) {
		for(FieldDeclaration field : fields) {
			for(Object fragment : field.fragments()) {
				JavaFieldInformation fieldInfo = new JavaFieldInformation(((VariableDeclarationFragment) fragment).getName().getIdentifier());
				
				Expression initializer = ((VariableDeclarationFragment) fragment).getInitializer();
				if(initializer != null)
					fieldInfo.setInitializer(initializer.toString());
				
				typeInfo.addField(fieldInfo);
			}
		}
	}
	
	
	
	private void addMethods(JavaTypeInformation typeInfo, MethodDeclaration[] methods) {
		for(MethodDeclaration method : methods) {
			JavaMethodInformation methodInfo = new JavaMethodInformation(method.getName().getIdentifier());
			
			Block body = method.getBody();
			if(body != null) {
				for(Object statement : body.statements()) {
					methodInfo.addStatement(statement.toString());
				}
			}
			
			for(Object listEntry : method.parameters()) {
				SingleVariableDeclaration parameter = (SingleVariableDeclaration) listEntry;
				
				org.eclipse.jdt.core.dom.Type parameterType = parameter.getType();
				if(parameterType instanceof SimpleType) {
					methodInfo.addParameterType(((SimpleType) parameterType).getName().getFullyQualifiedName());
				}
			}
			
			typeInfo.addMethod(methodInfo);
		}
	}
	
	
	
	
	
	private char[] getFileContent(IFile file) {
		String fileEnding = FileUtil.getLineSeparator(file);
		String content = "";
		
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(file.getContents(true), file.getCharset()));) {
			String line = reader.readLine();
			
			while (line != null) {
				content +=  line + fileEnding;
				line = reader.readLine();
			}
		} catch (Exception e) {
			DialogUtil.getInstance().openErrorDialog("An error occured while reading out the file " + file.getLocationURI() + ":\n\n" + e.getMessage());
		}
		
		return content.toCharArray();
	}

}






















