package de.tu_bs.cs.isf.temporalregulator3000.api.generation.sins;

import de.tu_bs.cs.isf.temporalregulator3000.api.generation.sins.util.APICodeGeneratorInfoWrapper;
import de.tu_bs.cs.isf.temporalregulator3000.api.generation.sins.util.APICodeGeneratorOppositeHandlingUtil;

public class APICodeGeneratorMultipleUnorderedReferenceUtil {
	
	public static String createContentsForGetMultipleUnorderedReferenceMethod(APICodeGeneratorInfoWrapper info) {
		return
				"	public List<" + info.nonTemporalType + "> get" + info.featureName + "At(" + info.nonTemporalAdapterInterface + " adapter, Date timePoint) {" + System.lineSeparator() + 
				"		EObject temporalMetaModelElement = getTemporalObjectFromCacheMap(adapter);" + System.lineSeparator() + 
				"" + System.lineSeparator() + 
				"		if(temporalMetaModelElement instanceof " + info.temporalType + ") {" + System.lineSeparator() + 
				"			List<" + info.associationReplacementInterfaceName + "> temporalReferences = ((" + info.temporalType + ") temporalMetaModelElement).getTemporal" + info.featureName + "Replacement();" + System.lineSeparator() + 
				"			List<" + info.associationReplacementInterfaceName + "> validTemporalReferences = EvolutionUtil.getValidTemporalElements(temporalReferences, timePoint);" + System.lineSeparator() + 
				"			" + System.lineSeparator() + 
				"			List<" + info.nonTemporalType + "> result = new LinkedList<>();" + System.lineSeparator() + 
				"			for(" + info.associationReplacementInterfaceName + " validTemporalReference : validTemporalReferences) {" + System.lineSeparator() + 
				"				" + info.temporalType + " temporalElement = validTemporalReference.get" + info.featureName + "();" + System.lineSeparator() + 
				"				result.add((" + info.nonTemporalType + ") getNonTemporalObjectFromCacheMap(temporalElement));" + System.lineSeparator() + 
				"			}" + System.lineSeparator() + 
				"			return result;" + System.lineSeparator() + 
				"		}" + System.lineSeparator() + 
				"" + System.lineSeparator() + 
				"		throw new UnsupportedOperationException(\"operation failed.\");" + System.lineSeparator() + 
				"	}";
	}
	
	public static String createContentsForAddMultipleUnorderedReferenceMethod(APICodeGeneratorInfoWrapper info) {
		String containmentCode = "";
		if(info.isContainment)
			containmentCode = System.lineSeparator() + 
			"					// Keep up the containment hierarchy" + System.lineSeparator() + 
			"					//if(temporalReferenceToAdd.eContainer() == null)" + System.lineSeparator() + 
			"						((" + info.temporalType + ") temporalMetaModelElement).getRelocated" + info.associationReplacementInterfaceName + "().add(temporalReferenceToAdd);" + System.lineSeparator();
		
		String oppositeReferenceMaintainCode = APICodeGeneratorOppositeHandlingUtil.createCodeForSetMultipleReference(info);
		
		String headLine;
		if(!info.hasOppositeReference)
			headLine = "	public void add" + info.featureName + "At(" + info.nonTemporalAdapterInterface + " adapter, List<Object> elements, int listIndexOfOperation, Date timePoint) {";
		else
			headLine = "	private void add" + info.featureName + "At(" + info.nonTemporalAdapterInterface + " adapter, List<Object> elements, int listIndexOfOperation, Date timePoint, boolean maintainOpposite) {";
		
		return headLine + System.lineSeparator() + 
				"		Collections.reverse(elements);" + System.lineSeparator() + 
				"		EObject temporalMetaModelElement = getTemporalObjectFromCacheMap(adapter);" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		if(temporalMetaModelElement instanceof " + info.temporalType + ") {" + System.lineSeparator() + 
				"			List<" + info.associationReplacementInterfaceName + "> temporalReferences = ((" + info.temporalType + ") temporalMetaModelElement).getTemporal" + info.featureName + "Replacement();" + System.lineSeparator() + 
				"			" + System.lineSeparator() + 
				"			for(Object commonReferenceToAdd : elements) {" + System.lineSeparator() + 
				"				Assert.isNotNull(commonReferenceToAdd);" + System.lineSeparator() + 
				"				if(commonReferenceToAdd instanceof " + info.nonTemporalType + ") {" + System.lineSeparator() + 
				"					" + info.temporalType + " temporalReferenceToAdd = (" + info.temporalType + ") getTemporalObjectFromCacheMap((EObject) commonReferenceToAdd);" + System.lineSeparator() + 
				"					" + containmentCode + 
				"					" + System.lineSeparator() + 
				"					" + oppositeReferenceMaintainCode + System.lineSeparator() + 
				"					" + info.associationReplacementInterfaceName + " newReferenceReplacement = " + info.temporalFactory + ".eINSTANCE.create" + info.associationReplacementInterfaceName + "();" + System.lineSeparator() + 
				"					newReferenceReplacement.setTemporalRegulatorValidSince(timePoint);" + System.lineSeparator() + 
				"					newReferenceReplacement.set" + info.featureName + "(temporalReferenceToAdd);" + System.lineSeparator() + 
				"					" + System.lineSeparator() + 
				"					temporalReferences.add(newReferenceReplacement);" + System.lineSeparator() + 
				"				}" + System.lineSeparator() + 
				"				else throw new RuntimeException(\"Wrong type of reference!\");" + System.lineSeparator() + 
				"			}" + System.lineSeparator() + 
				"		}" + System.lineSeparator() + 
				"	}";
	}
	
	public static String createContentsForRemoveMultipleUnorderedReferenceMethod(APICodeGeneratorInfoWrapper info) {
		String oppositeReferenceMaintainCode = APICodeGeneratorOppositeHandlingUtil.createCodeForRemoveMultipleReference(info);
		
		String headLine;
		if(!info.hasOppositeReference)
			headLine = "	public void remove" + info.featureName + "At(" + info.nonTemporalAdapterInterface + " adapter, List<Object> elements, Date timePoint) {";
		else
			headLine = "	private void remove" + info.featureName + "At(" + info.nonTemporalAdapterInterface + " adapter, List<Object> elements, Date timePoint, boolean maintainOpposite) {";
		
		return headLine + System.lineSeparator() + 
				"		EObject temporalMetaModelElement = getTemporalObjectFromCacheMap(adapter);" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		if(temporalMetaModelElement instanceof " + info.temporalType + ") {" + System.lineSeparator() + 
				"			List<" + info.associationReplacementInterfaceName + "> temporalReferences = ((" + info.temporalType + ") temporalMetaModelElement).getTemporal" + info.featureName + "Replacement();" + System.lineSeparator() + 
				"			List<" + info.associationReplacementInterfaceName + "> validTemporalReferences = EvolutionUtil.getValidTemporalElements(temporalReferences, timePoint);" + System.lineSeparator() + 
				"			" + System.lineSeparator() + 
				"			for(" + info.associationReplacementInterfaceName + " validTemporalReference : validTemporalReferences) {" + System.lineSeparator() + 
				"				for(Object commonReferenceToRemove : elements) {" + System.lineSeparator() + 
				"					Assert.isNotNull(commonReferenceToRemove);" + System.lineSeparator() + 
				"					if(commonReferenceToRemove instanceof " + info.nonTemporalType + ") {" + System.lineSeparator() + 
				"						if(getNonTemporalObjectFromCacheMap(validTemporalReference.get" + info.featureName + "()).equals(commonReferenceToRemove)) {" + System.lineSeparator() + 
				"							" + System.lineSeparator() + 
				"							" + oppositeReferenceMaintainCode + System.lineSeparator() + 
				"							validTemporalReference.setTemporalRegulatorValidUntil(timePoint);" + System.lineSeparator() + 
				"							TemporalUtil.doAdditionalTemporalCorrections(temporalMetaModelElement, temporalReferences, validTemporalReference, null, timePoint);" + System.lineSeparator() + 
				"						}" + System.lineSeparator() + 
				"					}" + System.lineSeparator() + 
				"					else throw new RuntimeException(\"Wrong type of reference!\");" + System.lineSeparator() + 
				"				}" + System.lineSeparator() + 
				"			}" + System.lineSeparator() + 
				"		}" + System.lineSeparator() + 
				"	}";
	}
	
}
