package de.tu_bs.cs.isf.temporalregulator3000.api.analysis.info;

import java.util.LinkedList;
import java.util.List;

public class JavaTypeInformation {
	
	public enum Type {
		CLASS,
		INTERFACE,
		ENUM
	};

	private List<String> imports_;
	private List<String> superInterfaces_;
	private List<JavaMethodInformation> methods_;
	private List<JavaFieldInformation> fields_;
	
	private Type type_;
	private JavaPackageInformation parentPackage_;
	private String superType_;
	
	private final String name_;
	
	
	
	@Override
	public String toString() {
		String type = this.type_ == Type.CLASS ? "Class" : this.type_ == Type.INTERFACE ? "Interface" : "Enum";
		return "Java " + type + " \"" + this.getName() + "\" inside " + this.parentPackage_;
	}
	
	
	
	@SuppressWarnings("unused")
	private JavaTypeInformation() {
		this.name_ = "<incorrectly instanciated type>";
	}
	
	public JavaTypeInformation(String name) {
		this.name_ = name;
		this.methods_ = new LinkedList<>();
		this.fields_ = new LinkedList<>();
		this.superInterfaces_ = new LinkedList<>();
		this.imports_ = new LinkedList<>();
	}
	
	public String getName() {
		return this.name_;
	}
	
	public String getQualifiedName() {
		if(this.parentPackage_ == null)
			throw new UnsupportedOperationException("No parent package set for type \"" + this.name_ + "\".");
		
		return this.parentPackage_.getQualifiedName() + "." + this.name_;
	}
	
	

	public void setParentPackage(JavaPackageInformation parent) {
		if(this.parentPackage_ == null)
			this.parentPackage_ = parent;
		else throw new UnsupportedOperationException("Setting the parent package of the type \"" + this.name_ + "\" to \"" + parent.getQualifiedName() + "\" failed. It was already assigned to the package \"" + this.parentPackage_.getQualifiedName() + "\".");
	}
	
	
	
	public void setType(Type type) {
		this.type_ = type;
	}
	
	public Type getType() {
		return this.type_;
	}
	
	
	
	public void setSuperType(String newSuper) {
		this.superType_ = newSuper;
	}
	
	public String getSuperTypes() {
		return this.superType_;
	}
	
	
	
	public void addSuperInterface(String newSuper) {
		this.superInterfaces_.add(newSuper);
	}
	
	public List<String> getSuperInterfaces() {
		return this.superInterfaces_;
	}
	
	
	
	public void addImport(String newImport) {
		this.imports_.add(newImport);
	}
	
	public List<String> getImports() {
		return this.imports_;
	}
	
	
	
	public void addMethod(JavaMethodInformation newMember) {
		newMember.setParentType(this);
		this.methods_.add(newMember);
	}
	
	public List<JavaMethodInformation> getMethods() {
		return this.methods_;
	}
	
	public JavaMethodInformation getMethod(String name) {
		for(JavaMethodInformation info : this.methods_)
			if(info.getName().equals(name))
				return info;
		return null;
	}
	
	
	
	public void addField(JavaFieldInformation newMember) {
		newMember.setParentType(this);
		this.fields_.add(newMember);
	}
	
	public List<JavaFieldInformation> getFields() {
		return this.fields_;
	}
	
	public JavaFieldInformation getField(String name) {
		for(JavaFieldInformation info : this.fields_)
			if(info.getName().equals(name))
				return info;
		return null;
	}
	
}



























