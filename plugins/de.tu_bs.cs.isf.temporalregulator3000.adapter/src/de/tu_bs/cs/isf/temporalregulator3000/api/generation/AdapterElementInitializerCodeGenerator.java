package de.tu_bs.cs.isf.temporalregulator3000.api.generation;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.emf.codegen.ecore.genmodel.GenClassifier;
import org.eclipse.emf.codegen.ecore.genmodel.GenPackage;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.JavaModelException;

import de.tu_bs.cs.isf.temporalregulator3000.api.CodeGenerator;
import de.tu_bs.cs.isf.temporalregulator3000.api.EClassInheritanceStore;
import de.tu_bs.cs.isf.temporalregulator3000.api.generation.util.CounterPartFinder;
import de.tu_bs.cs.isf.temporalregulator3000.core.StringUtil;

public class AdapterElementInitializerCodeGenerator extends BaseCodeGenerator {
	
	public String ADAPTER_INITILIAZER_NAME;

	private Set<GenClassifier> requiredGenClasses = new HashSet<>();
	private Set<GenClassifier> handledGenClasses = new HashSet<>();

	// ====
	// ====
	// ====
	// ====
	// ====
	// ====
	// ====
	// ====
	// ====
	// ====
	// ====
	// ====
	// ====
	// ====
	// TODO FIX MEEEEE PLEASE!! opposite references will result in double entries for many references... this needs to be fixed in the generation process!
	// ====
	// ====
	// ====
	// ====
	// ====
	// ====
	// ====
	// ====
	// ====
	// ====
	// ====
	// ====
	// ====
	// ====
	
	public AdapterElementInitializerCodeGenerator(IPackageFragment javaPackage, GenPackage temporalGenPackage, GenPackage nonTemporalGenPackage) {
		super(javaPackage, temporalGenPackage, nonTemporalGenPackage);
	}
	
	
	
	public void generateBaseCode() throws JavaModelException {
		ADAPTER_INITILIAZER_NAME = CodeGenerator.instance.getNamePrefix() + "AdapterElementInitializer";
		
		ICompilationUnit cu = javaPackage.createCompilationUnit(ADAPTER_INITILIAZER_NAME + ".java", "", true, null);
		CodeGenerator.instance.registerGeneratedSourceFile(cu.getCorrespondingResource());
		
		cu.createPackageDeclaration(javaPackage.getElementName(), null);
		cu.createImport("java.util.HashMap", null, null);
		cu.createImport("java.util.Map", null, null);
		cu.createImport("org.eclipse.emf.ecore.EObject", null, null);
		
		javaClass = cu.createType("public class " + ADAPTER_INITILIAZER_NAME + " {" + System.lineSeparator() + System.lineSeparator() + "}", null, false, null);

		javaClass.createField("Map<EObject, EObject> createdAdapterMap = new HashMap<>();", null, false, null);
		this.generateSingletonCode("");
	}



	public void generateAdapterInitCodeFor(EClass nonTemporalEClass, GenClassifier nonTemporalGenClassifier) throws JavaModelException {
		String qualifiedClassName = nonTemporalGenClassifier.getRawInstanceClassName();
		
		String contents = "	public " + qualifiedClassName + " createAdapterElementFor(" + qualifiedClassName + " originalElement) {" + System.lineSeparator();
		
		if(nonTemporalEClass.isAbstract())
			contents += generateAbstractAdapterInitCodeFor(nonTemporalEClass, nonTemporalGenClassifier);
		else
			contents += generateConcreteAdapterInitCodeFor(nonTemporalEClass, nonTemporalGenClassifier);
		
		contents += "	}";
		
		javaClass.createMethod(contents, null, false, null);
		handledGenClasses.add(nonTemporalGenClassifier);
	}
	
	
	
	public void generateAdapterInitCodeForExternalEClasses() throws JavaModelException {
		Set<GenClassifier> missingGenClasses = new HashSet<>();
		for(GenClassifier requiredGenClass : requiredGenClasses) {
			if(!handledGenClasses.contains(requiredGenClass)) {
				missingGenClasses.add(requiredGenClass);
			}
		}

		if(missingGenClasses.size() > 0) {
			for(GenClassifier missingGenClass : missingGenClasses) {
				EClassifier missingEClass = missingGenClass.getEcoreClassifier();
				generateAdapterInitCodeFor((EClass) missingEClass, missingGenClass);
			}
			
			generateAdapterInitCodeForExternalEClasses();
		}
	}



	private String generateAbstractAdapterInitCodeFor(EClass nonTemporalEClass, GenClassifier nonTemporalGenClassifier) {
		String contents = "";
		
		for(GenClassifier inheritingGenClass : EClassInheritanceStore.INSTANCE.getInheritingConcreteGenClasses(nonTemporalEClass)) {
			String inheritingGenClassInstanceName = inheritingGenClass.getRawInstanceClassName();
			contents +=
				"		if(originalElement instanceof " + inheritingGenClassInstanceName + ")" + System.lineSeparator() + 
				"			return createAdapterElementFor((" + inheritingGenClassInstanceName + ") originalElement);" + System.lineSeparator() + System.lineSeparator();
			
			requiredGenClasses.add(inheritingGenClass);
		}
		
		contents += "		throw new RuntimeException(\"Could not build an adapter element for following element with abstract EType:\\n\" + originalElement.toString());" + System.lineSeparator();
		
		return contents;
	}



	private String generateConcreteAdapterInitCodeFor(EClass nonTemporalEClass, GenClassifier nonTemporalGenClassifier) {
		String unqualifiedClassName = nonTemporalGenClassifier.getClassifierAccessorName();
		String qualifiedClassName = nonTemporalGenClassifier.getRawInstanceClassName();
		String qualifiedFactoryName = nonTemporalGenClassifier.getGenPackage().getImportedFactoryInterfaceName();
		
		String contents =
				"		// TODO CAREFUL: opposite references will result in double entries for many references... this needs to be fixed in the generation process!" + System.lineSeparator() + 
				"		" + qualifiedClassName + " adapterElement = (" + qualifiedClassName + ") createdAdapterMap.get(originalElement);" + System.lineSeparator() + 
				"		if(adapterElement != null)" + System.lineSeparator() + 
				"			return adapterElement;" + System.lineSeparator() + 
				"		" + System.lineSeparator() + 
				"		adapterElement = " + qualifiedFactoryName + ".eINSTANCE.create" + unqualifiedClassName + "();" + System.lineSeparator() + 
				"		createdAdapterMap.put(originalElement, adapterElement);" + System.lineSeparator() + System.lineSeparator();
		
		for(EStructuralFeature containedFeature : nonTemporalEClass.getEAllStructuralFeatures()) {
			String featureName = StringUtil.firstLetterToUpper(containedFeature.getName());
			String featureTypeName = CounterPartFinder.getQualifiedNonTemporalTypeName(containedFeature);
			
			if(containedFeature instanceof EAttribute) {
				if(!containedFeature.isMany()) {
					String keyword = featureTypeName.equals("boolean") ? "is" : "get";
					boolean needsNullCheck = true;
					if(containedFeature.getEType().getDefaultValue() != null && containedFeature.getEType().getInstanceClassName() != null)
						needsNullCheck = false;
					
					if(needsNullCheck)
						contents +=
							"		if(originalElement." + keyword + featureName + "() != null)" + System.lineSeparator();
					contents +=
							"			adapterElement.set" + featureName + "(originalElement." + keyword + featureName + "());" + System.lineSeparator() + System.lineSeparator();
				}
				else {
					contents +=
							"		for(" + featureTypeName + " attr : originalElement.get" + featureName + "())" + System.lineSeparator() + 
							"			adapterElement.get" + featureName + "().add(attr);" + System.lineSeparator() + System.lineSeparator();
				}
			}
			else if(containedFeature instanceof EReference) {
				if(!containedFeature.getEType().getName().equals("EObject"))
					requiredGenClasses.add(CounterPartFinder.getQualifiedNonTemporalGenClassifier(containedFeature.getEType()));
				
				if(!containedFeature.isMany()) {
					contents +=
							"		if(originalElement.get" + featureName + "() != null)" + System.lineSeparator() + 
							"			adapterElement.set" + featureName + "(createAdapterElementFor(originalElement.get" + featureName + "()));" + System.lineSeparator() + System.lineSeparator();
				}
				else {
					contents +=
							"		for(" + featureTypeName + " reference : originalElement.get" + featureName + "())" + System.lineSeparator() + 
							"			adapterElement.get" + featureName + "().add(createAdapterElementFor(reference));" + System.lineSeparator() + System.lineSeparator();
				}
			}
		}
		
		contents += "		return adapterElement;" + System.lineSeparator();
		
		return contents;
	}



}





















