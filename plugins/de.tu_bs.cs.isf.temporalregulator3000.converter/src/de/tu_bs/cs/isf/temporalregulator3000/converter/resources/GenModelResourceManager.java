package de.tu_bs.cs.isf.temporalregulator3000.converter.resources;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.codegen.ecore.generator.Generator;
import org.eclipse.emf.codegen.ecore.generator.GeneratorAdapterFactory;
import org.eclipse.emf.codegen.ecore.genmodel.GenJDKLevel;
import org.eclipse.emf.codegen.ecore.genmodel.GenModel;
import org.eclipse.emf.codegen.ecore.genmodel.GenModelFactory;
import org.eclipse.emf.codegen.ecore.genmodel.GenModelPackage;
import org.eclipse.emf.codegen.ecore.genmodel.GenPackage;
import org.eclipse.emf.codegen.ecore.genmodel.generator.GenBaseGeneratorAdapter;
import org.eclipse.emf.codegen.ecore.genmodel.generator.GenModelGeneratorAdapterFactory;
import org.eclipse.emf.common.util.BasicMonitor;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;

public class GenModelResourceManager {
	
	private GenModel originalGenModel;
	private GenModel derivedGenModel;
	
	public void searchOriginalGenModel(IFile originalEcoreFile) {
		IFile originalGenModelFile = originalEcoreFile.getParent().getFile(new Path(originalEcoreFile.getName().substring(0, originalEcoreFile.getName().length()-5) + "genmodel"));
		if(!originalGenModelFile.exists()) {
			List<IFile> genmodels = new ArrayList<>();
			try {
				for(IResource member : originalEcoreFile.getParent().members()) {
					if(member instanceof IFile) {
						if(member.getFileExtension().equals("genmodel")) {
							genmodels.add((IFile) member);
						}
					}
				}
			} catch (CoreException e) {
				throw new RuntimeException("An error occured while searching for the original genmodel:\n\n" + e.getMessage());
			}
			
			if(genmodels.size() == 0) {
				throw new RuntimeException("An error occured while searching for the original genmodel:\n\nNo genmodel provided in the folder \""+ originalEcoreFile.getParent().getProjectRelativePath() +"\".");
			}
			else if(genmodels.size() > 1) {
				throw new RuntimeException("The TemporalRegulator3000 found several genmodels laying besides your metamodel inside the folder \""+ originalEcoreFile.getParent().getProjectRelativePath() +"\".\n\n"
						+ "\"" + genmodels.get(0).getName() + "\" was picked as original Generator Model as it was found first.");
			}
			originalGenModelFile = genmodels.get(0);
		}
		
		this.originalGenModel = loadGenModel("platform:/resource/" + originalGenModelFile.getProject().getName() + "/" + originalGenModelFile.getProjectRelativePath());
	}

	public void createGenModel(String temporalProjectName, Resource temporalEcoreResource) {
		if(this.originalGenModel == null) {
			throw new RuntimeException("Error during Generator Model Creation!\n\nInvalid Ecore Model URI:\n" + "Couldn't find the non-temporal original Generator Model");
		}
		
		String ecoreLocation = temporalEcoreResource.getURI().toString();
		if(!ecoreLocation.endsWith("ecore")) {
			throw new RuntimeException("Error during Generator Model Creation!\n\nInvalid Ecore Model URI:\n" + ecoreLocation);
		}
		
		URI genModelURI = URI.createURI(ecoreLocation.substring(0, ecoreLocation.length()-5) + "genmodel");

//		EPackage.Registry.INSTANCE.containsKey(key);
		derivedGenModel = GenModelFactory.eINSTANCE.createGenModel();
		derivedGenModel.setComplianceLevel(GenJDKLevel.JDK70_LITERAL);
		
		derivedGenModel.setModelPluginID(temporalProjectName);
		derivedGenModel.setModelDirectory("/" + temporalProjectName + "/src-gen");
		derivedGenModel.setEditDirectory("/" + temporalProjectName + ".edit/src-gen");
		derivedGenModel.setEditorDirectory("/" + temporalProjectName + ".editor/src-gen");
		derivedGenModel.setTestsDirectory("/" + temporalProjectName + ".tests/src-gen");

		derivedGenModel.getForeignModel().add(temporalEcoreResource.getURI().lastSegment());
		derivedGenModel.setModelName(temporalEcoreResource.getURI().lastSegment().substring(0, temporalEcoreResource.getURI().lastSegment().length()-6));
//		derivedGenModel.setRootExtendsInterface(Constants.GEN_MODEL_EXTENDS_INTERFACE.getValue());
		derivedGenModel.setImportOrganizing(true);
		derivedGenModel.setCanGenerate(true);
		
		EPackage basePackage = (EPackage) temporalEcoreResource.getContents().get(0);
		derivedGenModel.initialize(Collections.singleton(basePackage));

		GenPackage rootGenPackage = (GenPackage) derivedGenModel.getGenPackages().get(0);
		rootGenPackage.setPrefix(basePackage.getNsPrefix());
		
		String originalBasePackage = this.originalGenModel.getGenPackages().get(0).getBasePackage();
		if(originalBasePackage != null)
			rootGenPackage.setBasePackage(originalBasePackage + ".temporal");
		else
			rootGenPackage.setBasePackage("temporal");
		
		GenModel temporalModelWithTemporalElementGenModel = loadGenModel("platform:/plugin/de.tu_bs.cs.isf.temporalregulator3000.core/model/TemporalModel.genmodel");
		if(temporalModelWithTemporalElementGenModel == null)
			throw new RuntimeException("Error during Generator Model Creation\n\nCould not load core temporal model \"platform:/plugin/de.tu_bs.cs.isf.temporalregulator3000.core/model/TemporalModel.genmodel\".");
		GenPackage temporalModelGenPackage = temporalModelWithTemporalElementGenModel.getGenPackages().get(0);
		derivedGenModel.getUsedGenPackages().add(temporalModelGenPackage);
		derivedGenModel.getUsedGenPackages().addAll(this.originalGenModel.getUsedGenPackages());

		try {
			final XMIResourceImpl genModelResource = new XMIResourceImpl(genModelURI);
			genModelResource.getDefaultSaveOptions().put(XMLResource.OPTION_ENCODING, "UTF-8");
			genModelResource.getContents().add(derivedGenModel);
			genModelResource.save(Collections.EMPTY_MAP);
		} catch (IOException e) {
			if (e instanceof FileNotFoundException) {
				throw new RuntimeException("Error during Generator Model Creation\n\nUnable to open output file " + genModelURI);
			} else {
				throw new RuntimeException("Error during Generator Model Creation\n\nUnexpected IO Exception writing " + genModelURI);
			}
		}
	}
	
	
	
	public GenModel getOriginalGenModel() {
		if(this.originalGenModel != null && this.derivedGenModel != null)
			return this.originalGenModel;
		
		throw new UnsupportedOperationException("Generation Models were not read and converted yet.");
	}
	
	public GenModel getDerivedGenModel() {
		if(this.originalGenModel != null && this.derivedGenModel != null)
			return this.derivedGenModel;
		
		throw new UnsupportedOperationException("Generation Models were not read and converted yet.");
	}
	
	
	
	private GenModel loadGenModel(String uriString) {
		ResourceSet set = new ResourceSetImpl();
		EcoreResourceFactoryImpl ecoreFactory = new EcoreResourceFactoryImpl();
		Resource.Factory.Registry registry = set.getResourceFactoryRegistry();
		
		Map<String, Object> map = registry.getExtensionToFactoryMap();
		map.put("ecore", ecoreFactory);
		map.put("genmodel", ecoreFactory);

		URI uri = URI.createURI(uriString);
		
		Resource res = set.getResource(uri, true);
		try {
			res.load(Collections.emptyMap());
		} catch (IOException e) {
			throw new RuntimeException("An error occured while setting the original genmodel:\n" + e.getMessage());
		}
		
		TreeIterator<EObject> list = res.getAllContents();
		while (list.hasNext()) {
			EObject obj = list.next();
			if (obj instanceof GenModel) {
				((GenModel) obj).reconcile();
				return (GenModel) obj;
			}
		}
		
		return null;
	}

	public void generateModelCode(IProgressMonitor monitor) throws FileNotFoundException {
		// Generate Code
		GeneratorAdapterFactory.Descriptor.Registry.INSTANCE.addDescriptor(GenModelPackage.eNS_URI, GenModelGeneratorAdapterFactory.DESCRIPTOR);

		// Create the generator and set the model-level input object.
		Generator generator = new Generator();
		generator.setInput(derivedGenModel);

		// Generator model code.
		generator.generate(derivedGenModel, GenBaseGeneratorAdapter.MODEL_PROJECT_TYPE, "model project", new BasicMonitor.Printing(new PrintStream(new FileOutputStream("Engineer.txt", true))));
	}
	
}


























