package de.tu_bs.cs.isf.temporalregulator3000.converter.execution.utility;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EPackage.Registry;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.impl.EcoreFactoryImpl;
import org.eclipse.emf.ecore.resource.Resource;

import de.tu_bs.cs.isf.temporalregulator3000.core.EcoreUtil;

public class ConversionUtil {
	private static List<EObject> allClasses;
	private static List<EObject> allReferences;
	private static List<EObject> allAttributes;

	public static Set<EObject> findElementsToConvert(Resource resource, List<EObject> searchedElements) {
		allClasses = new ArrayList<EObject>();
		allReferences = new ArrayList<EObject>();
		allAttributes = new ArrayList<EObject>();
		for(EObject root : resource.getContents()) {
			allClasses.addAll(EcoreUtil.getContainedElementsOfType(root, EcoreFactoryImpl.eINSTANCE.createEClass()));
			allReferences.addAll(EcoreUtil.getContainedElementsOfType(root, EcoreFactoryImpl.eINSTANCE.createEReference()));
			allAttributes.addAll(EcoreUtil.getContainedElementsOfType(root, EcoreFactoryImpl.eINSTANCE.createEAttribute()));
		}
		
		Set<EObject> elementsToConvert = new HashSet<>();
		
		for(EObject elementToFind : searchedElements) {
			if(elementToFind instanceof EClass) {
				for(EObject elementInResource : allClasses) {
					if(EcoreUtil.eClassesMatch((EClass) elementInResource, (EClass) elementToFind)) {
						elementsToConvert.add(elementInResource);
						break;
					}
				}
			}
			else if(elementToFind instanceof EReference) {
				for(EObject elementInResource : allReferences) {
					if(EcoreUtil.eStructuralFeaturesMatch((EStructuralFeature) elementInResource, (EStructuralFeature) elementToFind)) {
						elementsToConvert.add(elementInResource);
						break;
					}
				}
			}
			else if(elementToFind instanceof EAttribute) {
				for(EObject elementInResource : allAttributes) {
					if(EcoreUtil.eStructuralFeaturesMatch((EStructuralFeature) elementInResource, (EStructuralFeature) elementToFind)) {
						elementsToConvert.add(elementInResource);
						break;
					}
				}
			}
		}

		if(elementsToConvert.size() != searchedElements.size())
			return null;
		
		// Handle eOpposites, remove the non-containment opposite from the list of elements to convert (if existing)
		Set<EObject> elementsToConvertWithoutOpposites = new HashSet<>(elementsToConvert);
		for(EObject element : elementsToConvert) {
			if(element instanceof EReference) {
				EReference opposite = ((EReference) element).getEOpposite();
				if(opposite != null) {
					if(elementsToConvert.contains(opposite)) {
						if(opposite.isContainment())
							elementsToConvertWithoutOpposites.remove(element);
						else
							elementsToConvertWithoutOpposites.remove(opposite);
					}
				}
			}
		}
		return elementsToConvertWithoutOpposites;
	}
	
	
	
	public static EClass getTemporalCoreClass(String name) {
		Registry e = EPackage.Registry.INSTANCE;
		EPackage ePackage = e.getEPackage("https://www.tu-braunschweig.de/isf/TemporalRegulator/1.0");
		
		for(EClassifier classifier : ePackage.getEClassifiers()) {
			if(classifier instanceof EClass)
				if(classifier.getName().equals(name))
					return (EClass) classifier;
		}
		
		return null;
	}
	
}
