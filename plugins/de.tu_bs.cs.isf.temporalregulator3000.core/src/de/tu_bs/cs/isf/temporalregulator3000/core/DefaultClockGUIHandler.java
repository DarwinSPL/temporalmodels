package de.tu_bs.cs.isf.temporalregulator3000.core;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

public class DefaultClockGUIHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		
		DefaultClockGUIWindow clockGUI = new DefaultClockGUIWindow(window.getShell());
		clockGUI.open();
		
		return null;
	}
}



class DefaultClockGUIWindow extends InputDialog {
	
	private static SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

	public DefaultClockGUIWindow(Shell parentShell) {
		super(parentShell, "Temporal Regulator Default Clock Input", "Current time: " + ClockMechanism.instance.getTime() + System.lineSeparator() + "Set a new time below", formatter.format(ClockMechanism.instance.getTime()), new IInputValidator() {
			@Override
			public String isValid(String newText) {
				try {
					Date newDate = formatter.parse(newText);
					ClockMechanism.instance.setTimeDefault(newDate);
					return null;
				} catch (ParseException e) {
					return "Invalid date format, please use a date according to " + formatter.toPattern();
				}
			}
		});
	}
	
}