package de.tu_bs.cs.isf.temporalregulator3000.core;

import java.util.Date;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;

import de.tu_bs.cs.isf.temporalregulator3000.core.clockextension.IRegulatorClock;

public class ClockMechanism {
	
	public static ClockMechanism instance = new ClockMechanism();
	
	private ClockMechanism() { }
	
	// ==================================================================================== //
	
	private String EXTENSION_POINT_ID = "de.tu_bs.cs.isf.temporalregulator3000.core.clockextension";
	
	public Date getTime() {
		IExtensionRegistry registry = Platform.getExtensionRegistry();
		
		if(registry != null) {
			IExtensionPoint point = registry.getExtensionPoint(EXTENSION_POINT_ID);
			
			if (point == null) {
				System.err.println("Extension point \"" + EXTENSION_POINT_ID + "\" not found.");
				return getTimeDefault();
			}
			
			IExtension[] extensions = point.getExtensions();
			
			for(IExtension extension : extensions) {
				IConfigurationElement[] configurationElements = extension.getConfigurationElements();
//				String pluginId = extension.getNamespaceIdentifier();
				
				for (IConfigurationElement configurationElement : configurationElements) {
					String configurationElementName = configurationElement.getName();
					
					if (configurationElementName.equals("clock")) {
						Object clockExtension;
						try {
							clockExtension = configurationElement.createExecutableExtension("class");
							
							if (clockExtension instanceof IRegulatorClock) {
								return ((IRegulatorClock) clockExtension).getTime();
							}
						} catch (CoreException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
		
		return getTimeDefault();
	}
	
	
	
	private Date defaultTime = null;
	
	public Date getTimeDefault() {
		if(defaultTime == null)
			return new Date(System.currentTimeMillis());
		else
			return new Date(defaultTime.getTime());
	}
	
	public void setTimeDefault(Date newDefaultTime) {
		this.defaultTime = newDefaultTime;
	}
	
}
