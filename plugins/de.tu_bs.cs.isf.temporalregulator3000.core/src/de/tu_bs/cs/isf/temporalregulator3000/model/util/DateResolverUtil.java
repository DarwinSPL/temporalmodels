package de.tu_bs.cs.isf.temporalregulator3000.model.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateResolverUtil {

	
	protected static final String DEFAULT_DATE_FORMAT_STRING = "yyyy-MM-dd";
	protected static final String DEFAULT_DATE_FORMAT_TIME_STRING = DEFAULT_DATE_FORMAT_STRING + "'T'HH:mm:ss";
	
	protected static final SimpleDateFormat DEFAULT_DATE_FORMAT = new SimpleDateFormat(DEFAULT_DATE_FORMAT_STRING);
	protected static final SimpleDateFormat DEFAULT_DATE_FORMAT_TIME = new SimpleDateFormat(DEFAULT_DATE_FORMAT_TIME_STRING);
	
	private static final String NULL_VALUE_STRING = "eternity";
	
	public static Date resolveDate(String dateString) {
		if(dateString == null || dateString.equals("")) {
			return null;
		}
		
		String[] splits = dateString.split(":");
		
		SimpleDateFormat dateFormat;
		
		switch(splits.length) {
		case 1:
			dateFormat = DEFAULT_DATE_FORMAT;
			break;
		case 2:
			dateFormat = new SimpleDateFormat(DEFAULT_DATE_FORMAT_STRING+"'T'HH:mm");
			break;
		case 3:
			dateFormat = DEFAULT_DATE_FORMAT_TIME;
			break;
		default:
			dateFormat = DEFAULT_DATE_FORMAT;
			break;
		}
		
		return resolveDate(dateString, dateFormat);
	}
	
	public static Date resolveDate(String dateString, DateFormat customDateFormat) {		
		try {
			return customDateFormat.parse(dateString);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static String deresolveDate(Date date) {
		return deresolveDate(date, DEFAULT_DATE_FORMAT_TIME);
	}
	
	public static String deresolveDate(Date date, DateFormat customDateFormat) {
		if(date != null) {
			return customDateFormat.format(date);
		}
		else {
			return NULL_VALUE_STRING;
		}
		
	}
}
