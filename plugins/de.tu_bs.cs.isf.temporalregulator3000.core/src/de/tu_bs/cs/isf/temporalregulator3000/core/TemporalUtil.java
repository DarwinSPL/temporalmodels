package de.tu_bs.cs.isf.temporalregulator3000.core;

import java.util.Date;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import de.tu_bs.cs.isf.temporalregulator3000.temporalmodel.TemporalElement;

public class TemporalUtil {
	
	public static void doAdditionalTemporalCorrections(EObject container, List<? extends EObject> containingList, TemporalElement oldTempElement, TemporalElement newTempElement, Date timePoint) {
		if(oldTempElement != null)
			if(oldTempElement.getTemporalRegulatorValidSince().equals(oldTempElement.getTemporalRegulatorValidUntil()))
				containingList.remove(oldTempElement);

		if(oldTempElement != null && newTempElement != null) {
			newTempElement.setTemporalRegulatorValidUntil(oldTempElement.getTemporalRegulatorValidUntil());
			oldTempElement.setTemporalRegulatorValidUntil(timePoint);
		}
		
		if(container instanceof TemporalElement && newTempElement != null)
			if(newTempElement.getTemporalRegulatorValidUntil() != null)
				if(((TemporalElement) container).getTemporalRegulatorValidUntil().compareTo(newTempElement.getTemporalRegulatorValidUntil()) > 0)
					newTempElement.setTemporalRegulatorValidUntil(((TemporalElement) container).getTemporalRegulatorValidUntil());
	}
	
}
