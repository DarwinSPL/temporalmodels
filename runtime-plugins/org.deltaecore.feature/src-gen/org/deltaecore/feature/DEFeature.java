/**
 */
package org.deltaecore.feature;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DE Feature</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.deltaecore.feature.DEFeature#getName <em>Name</em>}</li>
 *   <li>{@link org.deltaecore.feature.DEFeature#getGroups <em>Groups</em>}</li>
 *   <li>{@link org.deltaecore.feature.DEFeature#getParentOfFeature <em>Parent Of Feature</em>}</li>
 * </ul>
 *
 * @see org.deltaecore.feature.DEFeaturePackage#getDEFeature()
 * @model
 * @generated
 */
public interface DEFeature extends DECardinalityBasedElement {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.deltaecore.feature.DEFeaturePackage#getDEFeature_Name()
	 * @model id="true" required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.deltaecore.feature.DEFeature#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Groups</b></em>' containment reference list.
	 * The list contents are of type {@link org.deltaecore.feature.DEGroup}.
	 * It is bidirectional and its opposite is '{@link org.deltaecore.feature.DEGroup#getParentOfGroup <em>Parent Of Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Groups</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Groups</em>' containment reference list.
	 * @see org.deltaecore.feature.DEFeaturePackage#getDEFeature_Groups()
	 * @see org.deltaecore.feature.DEGroup#getParentOfGroup
	 * @model opposite="parentOfGroup" containment="true"
	 * @generated
	 */
	EList<DEGroup> getGroups();

	/**
	 * Returns the value of the '<em><b>Parent Of Feature</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.deltaecore.feature.DEGroup#getFeatures <em>Features</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent Of Feature</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent Of Feature</em>' container reference.
	 * @see #setParentOfFeature(DEGroup)
	 * @see org.deltaecore.feature.DEFeaturePackage#getDEFeature_ParentOfFeature()
	 * @see org.deltaecore.feature.DEGroup#getFeatures
	 * @model opposite="features" transient="false"
	 * @generated
	 */
	DEGroup getParentOfFeature();

	/**
	 * Sets the value of the '{@link org.deltaecore.feature.DEFeature#getParentOfFeature <em>Parent Of Feature</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent Of Feature</em>' container reference.
	 * @see #getParentOfFeature()
	 * @generated
	 */
	void setParentOfFeature(DEGroup value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return (getMinCardinality() == 0 &amp;&amp; getMaxCardinality() == 1);'"
	 * @generated
	 */
	boolean isOptional();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return (getMinCardinality() == 1 &amp;&amp; getMaxCardinality() == 1);'"
	 * @generated
	 */
	boolean isMandatory();

} // DEFeature
