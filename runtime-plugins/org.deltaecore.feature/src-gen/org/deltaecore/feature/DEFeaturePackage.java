/**
 */
package org.deltaecore.feature;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.deltaecore.feature.DEFeatureFactory
 * @model kind="package"
 * @generated
 */
public interface DEFeaturePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "feature";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://deltaecore.org/feature/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "feature";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DEFeaturePackage eINSTANCE = org.deltaecore.feature.impl.DEFeaturePackageImpl.init();

	/**
	 * The meta object id for the '{@link org.deltaecore.feature.impl.DEFeatureModelImpl <em>Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.deltaecore.feature.impl.DEFeatureModelImpl
	 * @see org.deltaecore.feature.impl.DEFeaturePackageImpl#getDEFeatureModel()
	 * @generated
	 */
	int DE_FEATURE_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Root Feature</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DE_FEATURE_MODEL__ROOT_FEATURE = 0;

	/**
	 * The number of structural features of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DE_FEATURE_MODEL_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DE_FEATURE_MODEL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.deltaecore.feature.impl.DECardinalityBasedElementImpl <em>DE Cardinality Based Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.deltaecore.feature.impl.DECardinalityBasedElementImpl
	 * @see org.deltaecore.feature.impl.DEFeaturePackageImpl#getDECardinalityBasedElement()
	 * @generated
	 */
	int DE_CARDINALITY_BASED_ELEMENT = 3;

	/**
	 * The feature id for the '<em><b>Min Cardinality</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DE_CARDINALITY_BASED_ELEMENT__MIN_CARDINALITY = 0;

	/**
	 * The feature id for the '<em><b>Max Cardinality</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DE_CARDINALITY_BASED_ELEMENT__MAX_CARDINALITY = 1;

	/**
	 * The number of structural features of the '<em>DE Cardinality Based Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DE_CARDINALITY_BASED_ELEMENT_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>DE Cardinality Based Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DE_CARDINALITY_BASED_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.deltaecore.feature.impl.DEFeatureImpl <em>DE Feature</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.deltaecore.feature.impl.DEFeatureImpl
	 * @see org.deltaecore.feature.impl.DEFeaturePackageImpl#getDEFeature()
	 * @generated
	 */
	int DE_FEATURE = 1;

	/**
	 * The feature id for the '<em><b>Min Cardinality</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DE_FEATURE__MIN_CARDINALITY = DE_CARDINALITY_BASED_ELEMENT__MIN_CARDINALITY;

	/**
	 * The feature id for the '<em><b>Max Cardinality</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DE_FEATURE__MAX_CARDINALITY = DE_CARDINALITY_BASED_ELEMENT__MAX_CARDINALITY;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DE_FEATURE__NAME = DE_CARDINALITY_BASED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Groups</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DE_FEATURE__GROUPS = DE_CARDINALITY_BASED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Parent Of Feature</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DE_FEATURE__PARENT_OF_FEATURE = DE_CARDINALITY_BASED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>DE Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DE_FEATURE_FEATURE_COUNT = DE_CARDINALITY_BASED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Is Optional</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DE_FEATURE___IS_OPTIONAL = DE_CARDINALITY_BASED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Is Mandatory</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DE_FEATURE___IS_MANDATORY = DE_CARDINALITY_BASED_ELEMENT_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>DE Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DE_FEATURE_OPERATION_COUNT = DE_CARDINALITY_BASED_ELEMENT_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.deltaecore.feature.impl.DEGroupImpl <em>DE Group</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.deltaecore.feature.impl.DEGroupImpl
	 * @see org.deltaecore.feature.impl.DEFeaturePackageImpl#getDEGroup()
	 * @generated
	 */
	int DE_GROUP = 2;

	/**
	 * The feature id for the '<em><b>Min Cardinality</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DE_GROUP__MIN_CARDINALITY = DE_CARDINALITY_BASED_ELEMENT__MIN_CARDINALITY;

	/**
	 * The feature id for the '<em><b>Max Cardinality</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DE_GROUP__MAX_CARDINALITY = DE_CARDINALITY_BASED_ELEMENT__MAX_CARDINALITY;

	/**
	 * The feature id for the '<em><b>Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DE_GROUP__FEATURES = DE_CARDINALITY_BASED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Parent Of Group</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DE_GROUP__PARENT_OF_GROUP = DE_CARDINALITY_BASED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>DE Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DE_GROUP_FEATURE_COUNT = DE_CARDINALITY_BASED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Is Alternative</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DE_GROUP___IS_ALTERNATIVE = DE_CARDINALITY_BASED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Is Or</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DE_GROUP___IS_OR = DE_CARDINALITY_BASED_ELEMENT_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Is And</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DE_GROUP___IS_AND = DE_CARDINALITY_BASED_ELEMENT_OPERATION_COUNT + 2;

	/**
	 * The number of operations of the '<em>DE Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DE_GROUP_OPERATION_COUNT = DE_CARDINALITY_BASED_ELEMENT_OPERATION_COUNT + 3;


	/**
	 * Returns the meta object for class '{@link org.deltaecore.feature.DEFeatureModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model</em>'.
	 * @see org.deltaecore.feature.DEFeatureModel
	 * @generated
	 */
	EClass getDEFeatureModel();

	/**
	 * Returns the meta object for the containment reference '{@link org.deltaecore.feature.DEFeatureModel#getRootFeature <em>Root Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Root Feature</em>'.
	 * @see org.deltaecore.feature.DEFeatureModel#getRootFeature()
	 * @see #getDEFeatureModel()
	 * @generated
	 */
	EReference getDEFeatureModel_RootFeature();

	/**
	 * Returns the meta object for class '{@link org.deltaecore.feature.DEFeature <em>DE Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DE Feature</em>'.
	 * @see org.deltaecore.feature.DEFeature
	 * @generated
	 */
	EClass getDEFeature();

	/**
	 * Returns the meta object for the attribute '{@link org.deltaecore.feature.DEFeature#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.deltaecore.feature.DEFeature#getName()
	 * @see #getDEFeature()
	 * @generated
	 */
	EAttribute getDEFeature_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link org.deltaecore.feature.DEFeature#getGroups <em>Groups</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Groups</em>'.
	 * @see org.deltaecore.feature.DEFeature#getGroups()
	 * @see #getDEFeature()
	 * @generated
	 */
	EReference getDEFeature_Groups();

	/**
	 * Returns the meta object for the container reference '{@link org.deltaecore.feature.DEFeature#getParentOfFeature <em>Parent Of Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Parent Of Feature</em>'.
	 * @see org.deltaecore.feature.DEFeature#getParentOfFeature()
	 * @see #getDEFeature()
	 * @generated
	 */
	EReference getDEFeature_ParentOfFeature();

	/**
	 * Returns the meta object for the '{@link org.deltaecore.feature.DEFeature#isOptional() <em>Is Optional</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Optional</em>' operation.
	 * @see org.deltaecore.feature.DEFeature#isOptional()
	 * @generated
	 */
	EOperation getDEFeature__IsOptional();

	/**
	 * Returns the meta object for the '{@link org.deltaecore.feature.DEFeature#isMandatory() <em>Is Mandatory</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Mandatory</em>' operation.
	 * @see org.deltaecore.feature.DEFeature#isMandatory()
	 * @generated
	 */
	EOperation getDEFeature__IsMandatory();

	/**
	 * Returns the meta object for class '{@link org.deltaecore.feature.DEGroup <em>DE Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DE Group</em>'.
	 * @see org.deltaecore.feature.DEGroup
	 * @generated
	 */
	EClass getDEGroup();

	/**
	 * Returns the meta object for the containment reference list '{@link org.deltaecore.feature.DEGroup#getFeatures <em>Features</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Features</em>'.
	 * @see org.deltaecore.feature.DEGroup#getFeatures()
	 * @see #getDEGroup()
	 * @generated
	 */
	EReference getDEGroup_Features();

	/**
	 * Returns the meta object for the container reference '{@link org.deltaecore.feature.DEGroup#getParentOfGroup <em>Parent Of Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Parent Of Group</em>'.
	 * @see org.deltaecore.feature.DEGroup#getParentOfGroup()
	 * @see #getDEGroup()
	 * @generated
	 */
	EReference getDEGroup_ParentOfGroup();

	/**
	 * Returns the meta object for the '{@link org.deltaecore.feature.DEGroup#isAlternative() <em>Is Alternative</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Alternative</em>' operation.
	 * @see org.deltaecore.feature.DEGroup#isAlternative()
	 * @generated
	 */
	EOperation getDEGroup__IsAlternative();

	/**
	 * Returns the meta object for the '{@link org.deltaecore.feature.DEGroup#isOr() <em>Is Or</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Or</em>' operation.
	 * @see org.deltaecore.feature.DEGroup#isOr()
	 * @generated
	 */
	EOperation getDEGroup__IsOr();

	/**
	 * Returns the meta object for the '{@link org.deltaecore.feature.DEGroup#isAnd() <em>Is And</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is And</em>' operation.
	 * @see org.deltaecore.feature.DEGroup#isAnd()
	 * @generated
	 */
	EOperation getDEGroup__IsAnd();

	/**
	 * Returns the meta object for class '{@link org.deltaecore.feature.DECardinalityBasedElement <em>DE Cardinality Based Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DE Cardinality Based Element</em>'.
	 * @see org.deltaecore.feature.DECardinalityBasedElement
	 * @generated
	 */
	EClass getDECardinalityBasedElement();

	/**
	 * Returns the meta object for the attribute '{@link org.deltaecore.feature.DECardinalityBasedElement#getMinCardinality <em>Min Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min Cardinality</em>'.
	 * @see org.deltaecore.feature.DECardinalityBasedElement#getMinCardinality()
	 * @see #getDECardinalityBasedElement()
	 * @generated
	 */
	EAttribute getDECardinalityBasedElement_MinCardinality();

	/**
	 * Returns the meta object for the attribute '{@link org.deltaecore.feature.DECardinalityBasedElement#getMaxCardinality <em>Max Cardinality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Cardinality</em>'.
	 * @see org.deltaecore.feature.DECardinalityBasedElement#getMaxCardinality()
	 * @see #getDECardinalityBasedElement()
	 * @generated
	 */
	EAttribute getDECardinalityBasedElement_MaxCardinality();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	DEFeatureFactory getDEFeatureFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.deltaecore.feature.impl.DEFeatureModelImpl <em>Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.deltaecore.feature.impl.DEFeatureModelImpl
		 * @see org.deltaecore.feature.impl.DEFeaturePackageImpl#getDEFeatureModel()
		 * @generated
		 */
		EClass DE_FEATURE_MODEL = eINSTANCE.getDEFeatureModel();

		/**
		 * The meta object literal for the '<em><b>Root Feature</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DE_FEATURE_MODEL__ROOT_FEATURE = eINSTANCE.getDEFeatureModel_RootFeature();

		/**
		 * The meta object literal for the '{@link org.deltaecore.feature.impl.DEFeatureImpl <em>DE Feature</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.deltaecore.feature.impl.DEFeatureImpl
		 * @see org.deltaecore.feature.impl.DEFeaturePackageImpl#getDEFeature()
		 * @generated
		 */
		EClass DE_FEATURE = eINSTANCE.getDEFeature();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DE_FEATURE__NAME = eINSTANCE.getDEFeature_Name();

		/**
		 * The meta object literal for the '<em><b>Groups</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DE_FEATURE__GROUPS = eINSTANCE.getDEFeature_Groups();

		/**
		 * The meta object literal for the '<em><b>Parent Of Feature</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DE_FEATURE__PARENT_OF_FEATURE = eINSTANCE.getDEFeature_ParentOfFeature();

		/**
		 * The meta object literal for the '<em><b>Is Optional</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation DE_FEATURE___IS_OPTIONAL = eINSTANCE.getDEFeature__IsOptional();

		/**
		 * The meta object literal for the '<em><b>Is Mandatory</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation DE_FEATURE___IS_MANDATORY = eINSTANCE.getDEFeature__IsMandatory();

		/**
		 * The meta object literal for the '{@link org.deltaecore.feature.impl.DEGroupImpl <em>DE Group</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.deltaecore.feature.impl.DEGroupImpl
		 * @see org.deltaecore.feature.impl.DEFeaturePackageImpl#getDEGroup()
		 * @generated
		 */
		EClass DE_GROUP = eINSTANCE.getDEGroup();

		/**
		 * The meta object literal for the '<em><b>Features</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DE_GROUP__FEATURES = eINSTANCE.getDEGroup_Features();

		/**
		 * The meta object literal for the '<em><b>Parent Of Group</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DE_GROUP__PARENT_OF_GROUP = eINSTANCE.getDEGroup_ParentOfGroup();

		/**
		 * The meta object literal for the '<em><b>Is Alternative</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation DE_GROUP___IS_ALTERNATIVE = eINSTANCE.getDEGroup__IsAlternative();

		/**
		 * The meta object literal for the '<em><b>Is Or</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation DE_GROUP___IS_OR = eINSTANCE.getDEGroup__IsOr();

		/**
		 * The meta object literal for the '<em><b>Is And</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation DE_GROUP___IS_AND = eINSTANCE.getDEGroup__IsAnd();

		/**
		 * The meta object literal for the '{@link org.deltaecore.feature.impl.DECardinalityBasedElementImpl <em>DE Cardinality Based Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.deltaecore.feature.impl.DECardinalityBasedElementImpl
		 * @see org.deltaecore.feature.impl.DEFeaturePackageImpl#getDECardinalityBasedElement()
		 * @generated
		 */
		EClass DE_CARDINALITY_BASED_ELEMENT = eINSTANCE.getDECardinalityBasedElement();

		/**
		 * The meta object literal for the '<em><b>Min Cardinality</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DE_CARDINALITY_BASED_ELEMENT__MIN_CARDINALITY = eINSTANCE.getDECardinalityBasedElement_MinCardinality();

		/**
		 * The meta object literal for the '<em><b>Max Cardinality</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DE_CARDINALITY_BASED_ELEMENT__MAX_CARDINALITY = eINSTANCE.getDECardinalityBasedElement_MaxCardinality();

	}

} //DEFeaturePackage
