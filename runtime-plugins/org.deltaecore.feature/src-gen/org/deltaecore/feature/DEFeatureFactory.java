/**
 */
package org.deltaecore.feature;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.deltaecore.feature.DEFeaturePackage
 * @generated
 */
public interface DEFeatureFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DEFeatureFactory eINSTANCE = org.deltaecore.feature.impl.DEFeatureFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Model</em>'.
	 * @generated
	 */
	DEFeatureModel createDEFeatureModel();

	/**
	 * Returns a new object of class '<em>DE Feature</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DE Feature</em>'.
	 * @generated
	 */
	DEFeature createDEFeature();

	/**
	 * Returns a new object of class '<em>DE Group</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DE Group</em>'.
	 * @generated
	 */
	DEGroup createDEGroup();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	DEFeaturePackage getDEFeaturePackage();

} //DEFeatureFactory
