/**
 */
package org.deltaecore.feature.impl;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;

import org.deltaecore.feature.DEFeature;
import org.deltaecore.feature.DEFeaturePackage;
import org.deltaecore.feature.DEGroup;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DE Group</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.deltaecore.feature.impl.DEGroupImpl#getFeatures <em>Features</em>}</li>
 *   <li>{@link org.deltaecore.feature.impl.DEGroupImpl#getParentOfGroup <em>Parent Of Group</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DEGroupImpl extends DECardinalityBasedElementImpl implements DEGroup {
	/**
	 * The cached value of the '{@link #getFeatures() <em>Features</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeatures()
	 * @generated
	 * @ordered
	 */
	protected EList<DEFeature> features;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DEGroupImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DEFeaturePackage.Literals.DE_GROUP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DEFeature> getFeatures() {
		if (features == null) {
			features = new EObjectContainmentWithInverseEList<DEFeature>(DEFeature.class, this, DEFeaturePackage.DE_GROUP__FEATURES, DEFeaturePackage.DE_FEATURE__PARENT_OF_FEATURE);
		}
		return features;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DEFeature getParentOfGroup() {
		if (eContainerFeatureID() != DEFeaturePackage.DE_GROUP__PARENT_OF_GROUP) return null;
		return (DEFeature)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParentOfGroup(DEFeature newParentOfGroup, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newParentOfGroup, DEFeaturePackage.DE_GROUP__PARENT_OF_GROUP, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParentOfGroup(DEFeature newParentOfGroup) {
		if (newParentOfGroup != eInternalContainer() || (eContainerFeatureID() != DEFeaturePackage.DE_GROUP__PARENT_OF_GROUP && newParentOfGroup != null)) {
			if (EcoreUtil.isAncestor(this, newParentOfGroup))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newParentOfGroup != null)
				msgs = ((InternalEObject)newParentOfGroup).eInverseAdd(this, DEFeaturePackage.DE_FEATURE__GROUPS, DEFeature.class, msgs);
			msgs = basicSetParentOfGroup(newParentOfGroup, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DEFeaturePackage.DE_GROUP__PARENT_OF_GROUP, newParentOfGroup, newParentOfGroup));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isAlternative() {
		return (getMinCardinality() == 1 && getMaxCardinality() == 1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isOr() {
		if (getMinCardinality() != 1 || getMaxCardinality() != getFeatures().size()) {
			return false;
		}
		
		for (DEFeature feature : getFeatures()) {
			//If a feature of the group is mandatory, then it is really
			//not a special group but the cardinality is coincidence.
			if (feature.isMandatory()) {
				return false;
			}
		}
		
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isAnd() {
		int optionalFeatures = 0;
		int mandatoryFeatures = 0;
				
		for (DEFeature feature : getFeatures()) {
			if (feature.isOptional()) {
				optionalFeatures++;
			} else if (feature.isMandatory()) {
				mandatoryFeatures++;
			}
		}
				
		return (getMinCardinality() <= mandatoryFeatures && getMaxCardinality() >= (mandatoryFeatures + optionalFeatures));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DEFeaturePackage.DE_GROUP__FEATURES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getFeatures()).basicAdd(otherEnd, msgs);
			case DEFeaturePackage.DE_GROUP__PARENT_OF_GROUP:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetParentOfGroup((DEFeature)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DEFeaturePackage.DE_GROUP__FEATURES:
				return ((InternalEList<?>)getFeatures()).basicRemove(otherEnd, msgs);
			case DEFeaturePackage.DE_GROUP__PARENT_OF_GROUP:
				return basicSetParentOfGroup(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case DEFeaturePackage.DE_GROUP__PARENT_OF_GROUP:
				return eInternalContainer().eInverseRemove(this, DEFeaturePackage.DE_FEATURE__GROUPS, DEFeature.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DEFeaturePackage.DE_GROUP__FEATURES:
				return getFeatures();
			case DEFeaturePackage.DE_GROUP__PARENT_OF_GROUP:
				return getParentOfGroup();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DEFeaturePackage.DE_GROUP__FEATURES:
				getFeatures().clear();
				getFeatures().addAll((Collection<? extends DEFeature>)newValue);
				return;
			case DEFeaturePackage.DE_GROUP__PARENT_OF_GROUP:
				setParentOfGroup((DEFeature)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DEFeaturePackage.DE_GROUP__FEATURES:
				getFeatures().clear();
				return;
			case DEFeaturePackage.DE_GROUP__PARENT_OF_GROUP:
				setParentOfGroup((DEFeature)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DEFeaturePackage.DE_GROUP__FEATURES:
				return features != null && !features.isEmpty();
			case DEFeaturePackage.DE_GROUP__PARENT_OF_GROUP:
				return getParentOfGroup() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case DEFeaturePackage.DE_GROUP___IS_ALTERNATIVE:
				return isAlternative();
			case DEFeaturePackage.DE_GROUP___IS_OR:
				return isOr();
			case DEFeaturePackage.DE_GROUP___IS_AND:
				return isAnd();
		}
		return super.eInvoke(operationID, arguments);
	}

} //DEGroupImpl
