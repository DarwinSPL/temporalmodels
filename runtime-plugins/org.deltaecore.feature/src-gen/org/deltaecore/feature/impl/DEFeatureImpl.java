/**
 */
package org.deltaecore.feature.impl;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;

import org.deltaecore.feature.DEFeature;
import org.deltaecore.feature.DEFeaturePackage;
import org.deltaecore.feature.DEGroup;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DE Feature</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.deltaecore.feature.impl.DEFeatureImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.deltaecore.feature.impl.DEFeatureImpl#getGroups <em>Groups</em>}</li>
 *   <li>{@link org.deltaecore.feature.impl.DEFeatureImpl#getParentOfFeature <em>Parent Of Feature</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DEFeatureImpl extends DECardinalityBasedElementImpl implements DEFeature {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getGroups() <em>Groups</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroups()
	 * @generated
	 * @ordered
	 */
	protected EList<DEGroup> groups;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DEFeatureImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DEFeaturePackage.Literals.DE_FEATURE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DEFeaturePackage.DE_FEATURE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DEGroup> getGroups() {
		if (groups == null) {
			groups = new EObjectContainmentWithInverseEList<DEGroup>(DEGroup.class, this, DEFeaturePackage.DE_FEATURE__GROUPS, DEFeaturePackage.DE_GROUP__PARENT_OF_GROUP);
		}
		return groups;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DEGroup getParentOfFeature() {
		if (eContainerFeatureID() != DEFeaturePackage.DE_FEATURE__PARENT_OF_FEATURE) return null;
		return (DEGroup)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParentOfFeature(DEGroup newParentOfFeature, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newParentOfFeature, DEFeaturePackage.DE_FEATURE__PARENT_OF_FEATURE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParentOfFeature(DEGroup newParentOfFeature) {
		if (newParentOfFeature != eInternalContainer() || (eContainerFeatureID() != DEFeaturePackage.DE_FEATURE__PARENT_OF_FEATURE && newParentOfFeature != null)) {
			if (EcoreUtil.isAncestor(this, newParentOfFeature))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newParentOfFeature != null)
				msgs = ((InternalEObject)newParentOfFeature).eInverseAdd(this, DEFeaturePackage.DE_GROUP__FEATURES, DEGroup.class, msgs);
			msgs = basicSetParentOfFeature(newParentOfFeature, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DEFeaturePackage.DE_FEATURE__PARENT_OF_FEATURE, newParentOfFeature, newParentOfFeature));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isOptional() {
		return (getMinCardinality() == 0 && getMaxCardinality() == 1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isMandatory() {
		return (getMinCardinality() == 1 && getMaxCardinality() == 1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DEFeaturePackage.DE_FEATURE__GROUPS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getGroups()).basicAdd(otherEnd, msgs);
			case DEFeaturePackage.DE_FEATURE__PARENT_OF_FEATURE:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetParentOfFeature((DEGroup)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DEFeaturePackage.DE_FEATURE__GROUPS:
				return ((InternalEList<?>)getGroups()).basicRemove(otherEnd, msgs);
			case DEFeaturePackage.DE_FEATURE__PARENT_OF_FEATURE:
				return basicSetParentOfFeature(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case DEFeaturePackage.DE_FEATURE__PARENT_OF_FEATURE:
				return eInternalContainer().eInverseRemove(this, DEFeaturePackage.DE_GROUP__FEATURES, DEGroup.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DEFeaturePackage.DE_FEATURE__NAME:
				return getName();
			case DEFeaturePackage.DE_FEATURE__GROUPS:
				return getGroups();
			case DEFeaturePackage.DE_FEATURE__PARENT_OF_FEATURE:
				return getParentOfFeature();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DEFeaturePackage.DE_FEATURE__NAME:
				setName((String)newValue);
				return;
			case DEFeaturePackage.DE_FEATURE__GROUPS:
				getGroups().clear();
				getGroups().addAll((Collection<? extends DEGroup>)newValue);
				return;
			case DEFeaturePackage.DE_FEATURE__PARENT_OF_FEATURE:
				setParentOfFeature((DEGroup)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DEFeaturePackage.DE_FEATURE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case DEFeaturePackage.DE_FEATURE__GROUPS:
				getGroups().clear();
				return;
			case DEFeaturePackage.DE_FEATURE__PARENT_OF_FEATURE:
				setParentOfFeature((DEGroup)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DEFeaturePackage.DE_FEATURE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case DEFeaturePackage.DE_FEATURE__GROUPS:
				return groups != null && !groups.isEmpty();
			case DEFeaturePackage.DE_FEATURE__PARENT_OF_FEATURE:
				return getParentOfFeature() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case DEFeaturePackage.DE_FEATURE___IS_OPTIONAL:
				return isOptional();
			case DEFeaturePackage.DE_FEATURE___IS_MANDATORY:
				return isMandatory();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //DEFeatureImpl
