/**
 */
package org.deltaecore.feature.impl;

import org.deltaecore.feature.DECardinalityBasedElement;
import org.deltaecore.feature.DEFeature;
import org.deltaecore.feature.DEFeatureFactory;
import org.deltaecore.feature.DEFeatureModel;
import org.deltaecore.feature.DEFeaturePackage;
import org.deltaecore.feature.DEGroup;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DEFeaturePackageImpl extends EPackageImpl implements DEFeaturePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass deFeatureModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass deFeatureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass deGroupEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass deCardinalityBasedElementEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.deltaecore.feature.DEFeaturePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private DEFeaturePackageImpl() {
		super(eNS_URI, DEFeatureFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link DEFeaturePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static DEFeaturePackage init() {
		if (isInited) return (DEFeaturePackage)EPackage.Registry.INSTANCE.getEPackage(DEFeaturePackage.eNS_URI);

		// Obtain or create and register package
		Object registeredDEFeaturePackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		DEFeaturePackageImpl theDEFeaturePackage = registeredDEFeaturePackage instanceof DEFeaturePackageImpl ? (DEFeaturePackageImpl)registeredDEFeaturePackage : new DEFeaturePackageImpl();

		isInited = true;

		// Create package meta-data objects
		theDEFeaturePackage.createPackageContents();

		// Initialize created meta-data
		theDEFeaturePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theDEFeaturePackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(DEFeaturePackage.eNS_URI, theDEFeaturePackage);
		return theDEFeaturePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDEFeatureModel() {
		return deFeatureModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDEFeatureModel_RootFeature() {
		return (EReference)deFeatureModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDEFeature() {
		return deFeatureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDEFeature_Name() {
		return (EAttribute)deFeatureEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDEFeature_Groups() {
		return (EReference)deFeatureEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDEFeature_ParentOfFeature() {
		return (EReference)deFeatureEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getDEFeature__IsOptional() {
		return deFeatureEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getDEFeature__IsMandatory() {
		return deFeatureEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDEGroup() {
		return deGroupEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDEGroup_Features() {
		return (EReference)deGroupEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDEGroup_ParentOfGroup() {
		return (EReference)deGroupEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getDEGroup__IsAlternative() {
		return deGroupEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getDEGroup__IsOr() {
		return deGroupEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getDEGroup__IsAnd() {
		return deGroupEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDECardinalityBasedElement() {
		return deCardinalityBasedElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDECardinalityBasedElement_MinCardinality() {
		return (EAttribute)deCardinalityBasedElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDECardinalityBasedElement_MaxCardinality() {
		return (EAttribute)deCardinalityBasedElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DEFeatureFactory getDEFeatureFactory() {
		return (DEFeatureFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		deFeatureModelEClass = createEClass(DE_FEATURE_MODEL);
		createEReference(deFeatureModelEClass, DE_FEATURE_MODEL__ROOT_FEATURE);

		deFeatureEClass = createEClass(DE_FEATURE);
		createEAttribute(deFeatureEClass, DE_FEATURE__NAME);
		createEReference(deFeatureEClass, DE_FEATURE__GROUPS);
		createEReference(deFeatureEClass, DE_FEATURE__PARENT_OF_FEATURE);
		createEOperation(deFeatureEClass, DE_FEATURE___IS_OPTIONAL);
		createEOperation(deFeatureEClass, DE_FEATURE___IS_MANDATORY);

		deGroupEClass = createEClass(DE_GROUP);
		createEReference(deGroupEClass, DE_GROUP__FEATURES);
		createEReference(deGroupEClass, DE_GROUP__PARENT_OF_GROUP);
		createEOperation(deGroupEClass, DE_GROUP___IS_ALTERNATIVE);
		createEOperation(deGroupEClass, DE_GROUP___IS_OR);
		createEOperation(deGroupEClass, DE_GROUP___IS_AND);

		deCardinalityBasedElementEClass = createEClass(DE_CARDINALITY_BASED_ELEMENT);
		createEAttribute(deCardinalityBasedElementEClass, DE_CARDINALITY_BASED_ELEMENT__MIN_CARDINALITY);
		createEAttribute(deCardinalityBasedElementEClass, DE_CARDINALITY_BASED_ELEMENT__MAX_CARDINALITY);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		deFeatureEClass.getESuperTypes().add(this.getDECardinalityBasedElement());
		deGroupEClass.getESuperTypes().add(this.getDECardinalityBasedElement());

		// Initialize classes, features, and operations; add parameters
		initEClass(deFeatureModelEClass, DEFeatureModel.class, "DEFeatureModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDEFeatureModel_RootFeature(), this.getDEFeature(), null, "rootFeature", null, 1, 1, DEFeatureModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(deFeatureEClass, DEFeature.class, "DEFeature", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDEFeature_Name(), ecorePackage.getEString(), "name", null, 1, 1, DEFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDEFeature_Groups(), this.getDEGroup(), this.getDEGroup_ParentOfGroup(), "groups", null, 0, -1, DEFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDEFeature_ParentOfFeature(), this.getDEGroup(), this.getDEGroup_Features(), "parentOfFeature", null, 0, 1, DEFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getDEFeature__IsOptional(), ecorePackage.getEBoolean(), "isOptional", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getDEFeature__IsMandatory(), ecorePackage.getEBoolean(), "isMandatory", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(deGroupEClass, DEGroup.class, "DEGroup", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDEGroup_Features(), this.getDEFeature(), this.getDEFeature_ParentOfFeature(), "features", null, 1, -1, DEGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDEGroup_ParentOfGroup(), this.getDEFeature(), this.getDEFeature_Groups(), "parentOfGroup", null, 0, 1, DEGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getDEGroup__IsAlternative(), ecorePackage.getEBoolean(), "isAlternative", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getDEGroup__IsOr(), ecorePackage.getEBoolean(), "isOr", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getDEGroup__IsAnd(), ecorePackage.getEBoolean(), "isAnd", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(deCardinalityBasedElementEClass, DECardinalityBasedElement.class, "DECardinalityBasedElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDECardinalityBasedElement_MinCardinality(), ecorePackage.getEInt(), "minCardinality", null, 1, 1, DECardinalityBasedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDECardinalityBasedElement_MaxCardinality(), ecorePackage.getEInt(), "maxCardinality", null, 1, 1, DECardinalityBasedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //DEFeaturePackageImpl
