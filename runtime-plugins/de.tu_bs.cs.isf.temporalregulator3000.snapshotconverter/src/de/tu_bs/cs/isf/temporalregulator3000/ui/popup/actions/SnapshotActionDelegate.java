package de.tu_bs.cs.isf.temporalregulator3000.ui.popup.actions;

import java.io.IOException;
import java.util.Date;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.internal.ObjectPluginAction;

import org.deltaecore.feature.DEFeatureModel;
import org.deltaecore.temporal.FeatureAdapterElementDifferenceCalculator;
import org.deltaecore.temporal.FeatureAdapterElementInitializer;
import de.imotep.core.behavior.MStateMachine;
import de.imotep.core.temporal.BehaviorAdapterElementDifferenceCalculator;
import de.imotep.core.temporal.BehaviorAdapterElementInitializer;
import de.imotep.core.temporal.BehaviorEvolutionGateway;
import de.tu_bs.cs.isf.temporalregulator3000.core.ClockMechanism;
import de.tu_bs.cs.isf.temporalregulator3000.model.util.DateResolverUtil;

public class SnapshotActionDelegate implements IActionDelegate {

	private Shell shell;
	
	private Date currentTime;
	
	private ResourceSet resourceSet;
	private Resource[] resourcesToConvert;

	
	
	/**
	 * Constructor for Action1.
	 */
	public SnapshotActionDelegate() {
		super();
		currentTime = DateResolverUtil.resolveDate("2019-05-22T12:00:00");
		changeTime(0);
	}
	
	

	/**
	 * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
	}

	
	
	/**
	 * @see IActionDelegate#run(IAction)
	 */
	public void run(IAction action) {
		if(action instanceof ObjectPluginAction) {
			@SuppressWarnings("restriction")
			ISelection selection = ((ObjectPluginAction) action).getSelection();
			
			if(selection instanceof TreeSelection) {
				TreePath[] allTreePaths = ((TreeSelection) selection).getPaths();
//				if(allTreePaths.length == 1 && allTreePaths[0].getSegment(allTreePaths[0].getSegmentCount()-1) instanceof IFolder) {
//					try {
//						changeFileExtensionOfAllXMIFiles((IFolder) allTreePaths[0].getSegment(allTreePaths[0].getSegmentCount()-1), "behavior");
//					} catch (CoreException e) {
//						e.printStackTrace();
//					}
//					return;
//				}
				
				String resultMessage;
				try {
					resultMessage = loadResources(allTreePaths);
					if(resultMessage != null) {
						MessageDialog dialog = new MessageDialog(shell, "Snapshot Converter - Loading Models failed", null, resultMessage, MessageDialog.ERROR, new String [] { "Oohhh... :(" }, 0);
						dialog.open();
					}
				} catch (CoreException e) {
					e.printStackTrace();
				}
				
				resultMessage = createTemporalDeltaEcoreFeatureResource();
				if(resultMessage != null) {
					MessageDialog dialog = new MessageDialog(shell, "Snapshot Converter - Creating Temporal Resource failed", null, resultMessage, MessageDialog.ERROR, new String [] { "Oohhh... :(" }, 0);
					dialog.open();
				}
			}
		}
	}
	
	
	
	private void changeFileExtensionOfAllXMIFiles(IFolder sourceFolder, String newFileExtension) throws CoreException {
		IFolder destinationFolder = sourceFolder.getParent().getFolder(new Path(sourceFolder.getName() + "_" + newFileExtension));
		if(destinationFolder.exists())
			destinationFolder.delete(true, null);
		destinationFolder.create(true, true, null);
		
		for(IResource member : sourceFolder.members()) {
			member.copy(destinationFolder.getFullPath().append(member.getName().replaceAll("\\.xmi", "." + newFileExtension)), true, null);
		}
	}



	/**
	 * @see IActionDelegate#selectionChanged(IAction, ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
	}
	
	
	
	private String loadResources(TreePath[] allTreePaths) throws CoreException {
		resourcesToConvert = new Resource[allTreePaths.length];
		resourceSet = new ResourceSetImpl();
		
		int index = 0;
		
		for(TreePath treePath : allTreePaths) {
			Object file = treePath.getSegment(treePath.getSegmentCount()-1);
			
			if(file instanceof IFile) {
				IFile iFile = (IFile) file;
				
				URI uri = URI.createURI("platform:/resource/" + iFile.getProject().getName() + "/" + iFile.getProjectRelativePath().toString());
				Resource loadedRes = resourceSet.getResource(uri, true);
				
//				index = Integer.parseInt(iFile.getName().substring(10, 11));
//				if(resourcesToConvert[index] != null)
//					return "Please name your input files correctly!";
				
				resourcesToConvert[index++] = loadedRes;
			}
			
			if(file instanceof IFolder) {
				IFolder iFolder = (IFolder) file;
				IResource[] members = iFolder.members();

				int[] order = new int[] {0, 17, 8, 1, 15, 14, 4};
				order = new int[] {0, 17, 8};
				resourcesToConvert = new Resource[order.length];
				
				for(int i=0; i<order.length; i++) {
					IFile iFile = (IFile) members[order[i]];
					
					URI uri = URI.createURI("platform:/resource/" + iFile.getProject().getName() + "/" + iFile.getProjectRelativePath().toString());
					Resource loadedRes = resourceSet.getResource(uri, true);
					
					resourcesToConvert[i] = loadedRes;
				}
				
				return null;
			}
		}
		
		return null;
	}
	
	
	
	private String createTemporalDeltaEcoreFeatureResource() {
		System.out.println("Starting at " + currentTime.toString());
		DEFeatureModel combinedFeatureModel = null;
		
		FeatureAdapterElementDifferenceCalculator.instance.setComparator(new FeatureAdapterElementDifferenceCalculator.Comparator() {
			@Override
			public boolean equal(Object e1, Object e2) {
				if (e1 == null || e2 == null)
					return false;
				
				if(e1 instanceof org.deltaecore.feature.DEFeature && e2 instanceof org.deltaecore.feature.DEFeature)
					return ((org.deltaecore.feature.DEFeature) e1).getName().equals(((org.deltaecore.feature.DEFeature) e2).getName());
				
				return e1.equals(e2);
			}
		});
		
		for(Resource res : this.resourcesToConvert) {
			if(res.getContents().size() != 1 || !(res.getContents().get(0) instanceof DEFeatureModel))
				return "\"" + res.getURI() + "\" does not contain exactly one Feature Model...";

			if(combinedFeatureModel == null) {
				combinedFeatureModel = FeatureAdapterElementInitializer.instance.createAdapterElementFor((DEFeatureModel) res.getContents().get(0));
			}
			else {
				// Fast forward one day...
				changeTime(24);
				
				FeatureAdapterElementDifferenceCalculator.instance.applyDifferences(combinedFeatureModel, (DEFeatureModel) res.getContents().get(0));
			}
		}
		
		Resource temporalResource = resourceSet.createResource(URI.createURI(resourcesToConvert[0].getURI().toString().substring(0, 37) + "Combined.defeaturemodel"));
		temporalResource.getContents().add(combinedFeatureModel);
		
		try {
			temporalResource.save(null);
		} catch (IOException e) {
			return "Storing of the temporal resource failed: " + e.getMessage();
		}
		
		System.out.println("Finished at " + currentTime.toString());
		
		MessageDialog dialog = new MessageDialog(shell, "Conversion to temporal Model finished!", null, "DONE !!!!!!!!!!!!!!!!!!!!!!!!!1111111einseinself", MessageDialog.INFORMATION, new String [] { "So nice *.*" }, 0);
		dialog.open();
		return null;
	}
	
	
	
	private String createTemporalBehaviorMachineResource() {
		System.out.println("Starting at " + currentTime.toString());
		MStateMachine combinedAdapterMachine = null;
		
		BehaviorAdapterElementDifferenceCalculator.instance.setComparator(new BehaviorAdapterElementDifferenceCalculator.Comparator() {
			@Override
			public boolean equal(Object e1, Object e2) {
				if (e1 == null || e2 == null)
					return false;
				if (e1 instanceof de.imotep.core.datamodel.MNamedEntity && e1 instanceof de.imotep.core.datamodel.MNamedEntity) {
					de.imotep.core.datamodel.MNamedEntity entity1 = (de.imotep.core.datamodel.MNamedEntity) e1;
					de.imotep.core.datamodel.MNamedEntity entity2 = (de.imotep.core.datamodel.MNamedEntity) e2;
					
					if(entity1.getName().equals(entity2.getName())) {
						return true;
					}
				}
				return e1.equals(e2);
			}
		});
		
		for(Resource res : this.resourcesToConvert) {
			if(res.getContents().size() != 1 || !(res.getContents().get(0) instanceof MStateMachine))
				return "\"" + res.getURI() + "\" does not contain exactly one State Machine...";

			if(combinedAdapterMachine == null) {
				combinedAdapterMachine = BehaviorAdapterElementInitializer.instance.createAdapterElementFor((MStateMachine) res.getContents().get(0));
			}
			else {
				// Fast forward one day...
				changeTime(24);
				
				BehaviorAdapterElementDifferenceCalculator.instance.applyDifferences(combinedAdapterMachine, (MStateMachine) res.getContents().get(0));
			}
		}
		
		Resource temporalResource = resourceSet.createResource(URI.createURI(resourcesToConvert[0].getURI().toString().substring(0, resourcesToConvert[0].getURI().toString().length()-27) + "_temporal.behavior"));
		temporalResource.getContents().add(combinedAdapterMachine);
		
		try {
			temporalResource.save(null);
		} catch (IOException e) {
			return "Storing of the temporal resource failed: " + e.getMessage();
		}
		
		System.out.println("Finished at " + currentTime.toString());
		
		MessageDialog dialog = new MessageDialog(shell, "Conversion to temporal Model finished!", null, "DONE !!!!!!!!!!!!!!!!!!!!!!!!!1111111einseinself", MessageDialog.INFORMATION, new String [] { "So nice *.*" }, 0);
		dialog.open();
		return null;
	}



	private void changeTime(int deltaHours) {
		System.out.println("Forwarding " + deltaHours + " hours...");
		currentTime.setTime(currentTime.getTime() + deltaHours*1000*60*60);
		ClockMechanism.instance.setTimeDefault(new Date(currentTime.getTime()));
	}

}

















